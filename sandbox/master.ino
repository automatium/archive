#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>

#define SERVER_ADDRESS 1
#define CLIENT_ADDRESS 2

#define PROG_CHUNK 32

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, SERVER_ADDRESS);

void setup() {
  Serial.begin(115200);

  if (PROG_CHUNK > RH_RF95_MAX_MESSAGE_LEN) {
    Serial.println("Chunks are too large!");
  }

  if (!manager.init())
    Serial.println("init failed");
  else
    Serial.print("> ");
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf =
  // 128chips/symbol, CRC on

  // initialize digital pin LED as an output.
  // pinMode(LED, OUTPUT);
}

void dumpBuf(void *buf, uint16_t len) {
  for (uint16_t i = 0; i < len; i++) {
    Serial.print(((uint8_t *)buf)[i], HEX);
    Serial.print(".");
  }
  Serial.println("");
}

#define CMD_PROG "PROG_RAW"
#define CMD_ABORT "ABORT"
#define ACK_MSG "ACK"
#define EOF_MSG "EOF"

uint8_t prog_msg[] = "PROG";
// Dont put this on the stack:
uint8_t buf[PROG_CHUNK];
uint32_t buf_cursor = 0;

byte byteRead;

void loop() {
  if (Serial.available()) {
    // read the char
    byteRead = Serial.read();

    if (byteRead == 13) {
      // send crlf
      Serial.println("");
      // send the message wirelessly
      dumpBuf(&buf, buf_cursor);
      if (manager.sendtoWait(buf, buf_cursor, CLIENT_ADDRESS)) {
        Serial.print("> ");
      } else {
        Serial.println("send failed");
        Serial.print("> ");
      }
      // reset the cursor
      buf_cursor = 0;
    } else if (buf_cursor == RH_RF95_MAX_MESSAGE_LEN) {
      Serial.write('*');
    } else {
      // echo the char
      Serial.write(byteRead);

      // add the char to the buffer
      buf[buf_cursor] = byteRead;
      buf_cursor++;

      // check for prog command
      if ((buf_cursor == 8) && (memcmp(&buf, &CMD_PROG, 8) == 0)) {
        // newline after prompt
        Serial.println("");

        // send PROG command to remote
        if (manager.sendtoWait(prog_msg, 4, CLIENT_ADDRESS)) {
          Serial.println("INIT_OK");
        } else {
          Serial.println("INIT_ERR");
          return;
        }

        uint8_t buf[PROG_CHUNK];
        uint8_t buf_cursor = 0;

        // timeout timer
        uint32_t now = millis();

        while (1) {
          if (Serial.available()) {
            buf[buf_cursor] = Serial.read();

            buf_cursor++;
            now = millis();

            if (buf_cursor == PROG_CHUNK + 1) {
              dumpBuf(&buf, buf_cursor);
              if (manager.sendtoWait(buf, buf_cursor, CLIENT_ADDRESS)) {
                // request more serial data
                Serial.println(ACK_MSG);

                // reset the cursor
                buf_cursor = 0;
              } else {
                Serial.println("SEND_ERR");
              }
            }
          } else if ((millis() - now) > 2000) {
            // no data recieved for 5 seconds
            dumpBuf(&buf, buf_cursor);

            // send the remaining buffer contents
            if (manager.sendtoWait(buf, buf_cursor, CLIENT_ADDRESS)) {
              if (manager.sendtoWait(EOF_MSG, 3, CLIENT_ADDRESS)) {
                Serial.println("ACK");
                Serial.println("END_OK");
              } else {
                Serial.println("END_ERR");
              }
            } else {
              Serial.println("SEND_ERR");
            }

            // return to normal mode
            buf_cursor = 0;
            Serial.print("> ");
            Serial.print(buf_cursor) break;
          }
        }
      }
    }
  }
}
