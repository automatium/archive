# References

Temperature Sensor + Arduino
https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

SHT1x temperature sensor:
https://github.com/keito/pi-sht1x
https://www.adafruit.com/product/1298

Capacitive soil moisture sensor:
https://www.tindie.com/products/pinotech/soilwatch-10-soil-moisture-sensor/

Code to read watermark sensor:
https://github.com/ReiniervdL/Vinduino/blob/master/Vinduino-R_sensor_test/Vinduino-R_sensor_test.ino

How to read watermark sensors:
https://github.com/ReiniervdL/Vinduino/blob/master/Vinduino-R_sensor_test/Vinduino-R_sensor_test.ino

LoRaWAN Modules:
http://www.embeddedworks.net/lpwan102.html?query=82LoRa%20Proprietary%20(915MHz),,
