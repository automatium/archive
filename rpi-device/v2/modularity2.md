# Modularity 2.0

The pinout:
1. 3v
2. UART TX
3. UART RX
4. GND
5. Line voltage (negotiated)
6. CS (also used for interrupt)

Instead of buying outdoor connectors, I plan to use regular indoor ones and terminate them inside a door.

Pick a cable from here: https://dirtypcbs.com/store/cables/about.  Maybe PH 2.0mm or 51021 1.25mm

A typical setup would be:

* ATMEGA328PB-AU ($1.24)
* Connectors + wiring harness
