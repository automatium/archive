# Maybe modular would still be a good thing

I've determined that trying to create a standardized interface for all modules at this point would make things too complicated.

But what about having a module-specific interface?  It would still net me most of the benefits:

* Easier testability
* Cheaper to replace problematic modules

## What's left

* Source parts for power distribution
* Regulator + MOSFET + Caps
* Design the integrated board.


**Saving for later**

* Finish moisture sensor
* 1-wire input
* SHT10 input
* Quiic input

## Modules

**Power distribution**

* Measure current in all three directions.
* Battery voltage.

1. 3v
2. GND
3. SDA
4. SCL
5. VBAT

**Valve Output**

Done

1. VBAT (12-15v)
2. 3v (should go to GPIO pin as EN)
3. GND
4. A_PH
5. A_EN
6. B_PH
7. B_EN

**Moisture Sensor**

1. 3v
2. 5v
2. GND
3. SCL
4. SDA
5. Enable

**1-wire input**

For remote temperature probes and the like.
4 ports and a pull-up resistor.

1. 3v
2. data
3. gnd

No enable pin, as I don't expect power consumption to be very high

**SHT10 input**

SHT10

Very low power consumption.  No enable neccesary.

**Qwiic**

1. SDA
2. SCL
3. 3v
4. gnd

We definitely need an enable output.

**Real Time Clock**

Done

1. 3v
2. SCL
3. SDA
4. gnd
5. EN
6. INT
7. PSW

No enable, haha.  It is the enable.

**Power Regulator**

Made by Microchip.

1. VIN
2. VOUT
3. GND


## Retired Modules

**Moisture Sensor (old style)**

https://github.com/ReiniervdL/Vinduino/blob/master/Vinduino-R_sensor_test/Vinduino-R_sensor_test.ino

Passing on this one for now

1. 5v
2. 3v
3. gnd
4. a_ph
5. a_en
6. b_ph
7. b_en
8. SDA
9. SCL

## Inter-module connections
