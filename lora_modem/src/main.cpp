#define SERIAL_PROTOCOL_VERSION 0

// #include "Buffer.h"
#include <Arduino.h>
#include "RadioConfig.cpp"
// #include "RollingBuffer.cpp"
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>
#include <ArduinoJson.h>
#include <Base64.h>

#if defined (MOTEINO_M0)
  #if defined(SERIAL_PORT_USBVIRTUAL)
    #define Serial SERIAL_PORT_USBVIRTUAL // Required for Serial on Zero based boards
  #endif
#endif

#define LORA_ADDRESS 1

bool init_status = false;
RH_RF95 driver(A2, 9);
RHReliableDatagram manager(driver, LORA_ADDRESS);

void setup()
{
  Serial.begin(115200);

  Serial.println("INIT_START");

  init_status = manager.init();


  if (init_status)
  {
    Serial.println("INIT_OK");
    
  }
  else {
    Serial.println("INIT_ERR");
  }
    

  // configure driver/manager
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  manager.setRetries(50);
}

// Don't put this on the stack:
uint8_t rx_buf[RH_RF95_MAX_MESSAGE_LEN];

void loop()
{
  // read next message from serial
  const size_t capacity = JSON_OBJECT_SIZE(10) + 100;
  DynamicJsonDocument doc(capacity);
  deserializeJson(doc, Serial);

  // match type
  // const char* type = doc["type"];
  if (strcmp(doc["type"], "Version") == 0) {
    Serial.println(SERIAL_PROTOCOL_VERSION);
  } 
  else if (strcmp(doc["type"], "InitStatus") == 0) {
    Serial.println(init_status);
  }
  else if (strcmp(doc["type"], "SendPacket") == 0) {
    
    uint8_t dst = doc["dst"];
    uint8_t len = doc["len"];
    const char* msg = doc["data"];
    uint8_t* buf = (uint8_t*) msg;
    // Serial.print("Sending packet to ");
    // Serial.print(dst);
    // Serial.print(" with length ");
    // Serial.print(len);
    // Serial.print(": ");
    // Serial.println((char*) buf);
    bool send_status = manager.sendtoWait(buf, len, dst);
    Serial.println(send_status);
  }
  else if (strcmp(doc["type"], "RxWait") == 0) {
    uint8_t len;
    uint16_t timeout = doc["timeout"];
    uint8_t from;
    uint8_t to;
    uint8_t id;
    uint8_t flags;

    bool rx_status = manager.recvfromAckTimeout(rx_buf, &len, timeout, &from, &to, &id, &flags);

    // respond
    const size_t capacity = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(6) + 110;
    StaticJsonDocument<capacity> doc;

    if (rx_status) {
      // Serial.print("got len=0x");
      // Serial.print(len, HEX);
      // Serial.print(" reply from 0x");
      // Serial.print(from, HEX);
      // Serial.print(": ");
      // Serial.println((char*)rx_buf);

      // status = ok
      doc["status"] = "Ok";

      // embed the meta-data
      JsonObject data = doc.createNestedObject("data");
      data["src"] = from;
      data["dst"] = to;
      data["len"] = len;
      data["id"] = id;
      data["flags"] = flags;

      // embed the message contents as base64
      int encodedLength = Base64.encodedLength(len);
      char encodedString[encodedLength];
      Base64.encode(encodedString, (char*)rx_buf, len);
      data["data"] = encodedString;
    } else {
      // status = error
      doc["status"] = "Err";
      // we have no way of knowing which error occured
      doc["error"] = nullptr;
    }

    // serialize
      serializeJson(doc, Serial);
      Serial.println("");
  }
}