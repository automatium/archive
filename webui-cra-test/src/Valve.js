import React from "react"

import Header from "./Header"

const Valve = ({ match }) => {
  const id = match.params.valve_id

  return (
    <div>
      <Header title="Sweet Pea Overhead" back="/" />
    </div>
  )
}

export default Valve
