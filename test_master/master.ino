// #include "Buffer.h"
#include "../lib/RadioConfig.cpp"
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>

#define SERVER_ADDRESS 1
#define CLIENT_ADDRESS 2

#define PROG_CHUNK 200

// commands and messages
#define CMD_MAX_LEN 25

#define IPS(i) i, sizeof(i) - 1

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, SERVER_ADDRESS);

void setup() {
  Serial.begin(115200);

  // init manager
  if (!manager.init())
    Serial.println("INIT_ERR");
  else
    Serial.println("INIT_OK");

  // configure driver
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  driver.setPreambleLength(TX_PREAMBLE, ACK_PREAMBLE);
}

bool lastState = 0;

void loop() {

  if (Serial.available()) {
    if (Serial.read() == 13) {

      uint8_t msg[] = "S1";

      if (lastState) {
        msg[0] = 'E';
        msg[1] = '1';
      }

      unsigned long start = millis();
      bool status = true;

      Serial.print("Sending ");
      Serial.println((char *)msg);

      while (millis() - start < 10000) {
        if (!manager.sendto(msg, 2, CLIENT_ADDRESS)) {
          status = false;
          break;
        } else {
          manager.waitPacketSent();
        }
      }

      if (status) {
        Serial.println("SEND_OK");
        lastState = !lastState;
      } else {
        Serial.println("SEND_ERR");
      }
    }

    Serial.println("");
  }
}
