<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="128bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="129" name="129bmp" color="2" fill="4" visible="yes" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="MF_IC_Digital">
<packages>
<package name="TQFP32">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard TQFP-32 Package.&lt;br/&gt;</description>
<smd name="P$1" x="-4.5" y="2.8" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$2" x="-4.5" y="2" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$3" x="-4.5" y="1.2" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$4" x="-4.5" y="0.4" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$5" x="-4.5" y="-0.4" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$6" x="-4.5" y="-1.2" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$7" x="-4.5" y="-2" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$8" x="-4.5" y="-2.8" dx="1.4" dy="0.4" layer="1"/>
<smd name="P$9" x="-2.8" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$10" x="-2" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$11" x="-1.2" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$12" x="-0.4" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$13" x="0.4" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$14" x="1.2" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$15" x="2" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$16" x="2.8" y="-4.5" dx="1.4" dy="0.4" layer="1" rot="R90"/>
<smd name="P$17" x="4.5" y="-2.8" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$18" x="4.5" y="-2" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$19" x="4.5" y="-1.2" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$20" x="4.5" y="-0.4" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$21" x="4.5" y="0.4" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$22" x="4.5" y="1.2" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$23" x="4.5" y="2" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$24" x="4.5" y="2.8" dx="1.4" dy="0.4" layer="1" rot="R180"/>
<smd name="P$25" x="2.8" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$26" x="2" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$27" x="1.2" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$28" x="0.4" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$29" x="-0.4" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$30" x="-1.2" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$31" x="-2" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<smd name="P$32" x="-2.8" y="4.5" dx="1.4" dy="0.4" layer="1" rot="R270"/>
<wire x1="-5.5" y1="5.5" x2="5.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="5.5" x2="5.5" y2="-5.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-5.5" x2="-5.5" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-5.5" x2="-5.5" y2="5.5" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-5.5" y="5.5"/>
<vertex x="-4" y="5.5"/>
<vertex x="-4" y="4"/>
<vertex x="-5.5" y="4"/>
</polygon>
<wire x1="-3" y1="-3" x2="-3" y2="2" width="0.127" layer="21"/>
<wire x1="-3" y1="2" x2="-2" y2="3" width="0.127" layer="21"/>
<wire x1="-2" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<text x="-5.5" y="6" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="ATMEGA328P">
<description>&lt;b&gt;Manufacturer Part #:&lt;/b&gt; ATMEGA328P&lt;br/&gt;
&lt;b&gt;Manufacturer:&lt;/b&gt; Atmel&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; 8-bit Microcontrollers - MCU 32KB In-system Flash 20MHz 1.8V-5.5V&lt;br/&gt;</description>
<pin name="VCC" x="-2.54" y="-2.54" visible="off" length="short" direction="pwr"/>
<pin name="GND" x="-2.54" y="-7.62" visible="off" length="short" direction="pwr"/>
<pin name="AREF" x="-2.54" y="-17.78" visible="off" length="short" direction="pwr"/>
<pin name="AVCC" x="-2.54" y="-22.86" visible="off" length="short" direction="pwr"/>
<text x="0.762" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">VCC</text>
<text x="0.762" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">VSS (GND)</text>
<text x="0.762" y="-17.78" size="1.016" layer="97" font="vector" align="center-left">AREF</text>
<text x="0.762" y="-22.86" size="1.016" layer="97" font="vector" align="center-left">AVCC</text>
<pin name="PB0" x="33.02" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="PB1" x="33.02" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="PB2" x="33.02" y="-7.62" visible="off" length="short" rot="R180"/>
<pin name="PB3" x="33.02" y="-10.16" visible="off" length="short" rot="R180"/>
<pin name="PB4" x="33.02" y="-12.7" visible="off" length="short" rot="R180"/>
<pin name="PB5" x="33.02" y="-15.24" visible="off" length="short" rot="R180"/>
<pin name="PB6" x="33.02" y="-17.78" visible="off" length="short" rot="R180"/>
<pin name="PB7" x="33.02" y="-20.32" visible="off" length="short" rot="R180"/>
<pin name="ADC7" x="-2.54" y="-53.34" visible="off" length="short"/>
<pin name="ADC6" x="-2.54" y="-50.8" visible="off" length="short"/>
<pin name="PC0" x="33.02" y="-25.4" visible="off" length="short" rot="R180"/>
<pin name="PC1" x="33.02" y="-27.94" visible="off" length="short" rot="R180"/>
<pin name="PC2" x="33.02" y="-30.48" visible="off" length="short" rot="R180"/>
<pin name="PC3" x="33.02" y="-33.02" visible="off" length="short" rot="R180"/>
<pin name="PC4" x="33.02" y="-35.56" visible="off" length="short" rot="R180"/>
<pin name="PC5" x="33.02" y="-38.1" visible="off" length="short" rot="R180"/>
<pin name="PD0" x="33.02" y="-45.72" visible="off" length="short" rot="R180"/>
<pin name="PD1" x="33.02" y="-48.26" visible="off" length="short" rot="R180"/>
<pin name="PD2" x="33.02" y="-50.8" visible="off" length="short" rot="R180"/>
<pin name="PD3" x="33.02" y="-53.34" visible="off" length="short" rot="R180"/>
<pin name="PD4" x="33.02" y="-55.88" visible="off" length="short" rot="R180"/>
<pin name="PD5" x="33.02" y="-58.42" visible="off" length="short" rot="R180"/>
<pin name="PD6" x="33.02" y="-60.96" visible="off" length="short" rot="R180"/>
<pin name="PD7" x="33.02" y="-63.5" visible="off" length="short" rot="R180"/>
<text x="0.762" y="-50.8" size="1.016" layer="97" font="vector" align="center-left">ADC6</text>
<text x="0.762" y="-53.34" size="1.016" layer="97" font="vector" align="center-left">ADC7</text>
<text x="29.718" y="-20.32" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT7/XTAL2/TOSC2) PB7</text>
<text x="29.718" y="-17.78" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT6/XTAL1/TOSC1) PB6</text>
<text x="29.718" y="-15.24" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT5/SCK) PB5</text>
<text x="29.718" y="-12.7" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT4/MISO) PB4</text>
<text x="29.718" y="-10.16" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT3/OC2A/MOSI) PB3</text>
<text x="29.718" y="-7.62" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT2/SS/OC1B) PB2</text>
<text x="29.718" y="-5.08" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT1/OC1A) PB1</text>
<text x="29.718" y="-2.54" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT0/CLKO/ICP1) PB0</text>
<text x="29.718" y="-38.1" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT13/SCL/ADC5) PC5</text>
<text x="29.718" y="-35.56" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT12/SDA/ADC4) PC4</text>
<text x="29.718" y="-33.02" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT11/ADC3) PC3</text>
<text x="29.718" y="-30.48" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT10/ADC2) PC2</text>
<text x="29.718" y="-27.94" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT9/ADC1) PC1</text>
<text x="29.718" y="-25.4" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT8/ADC0) PC0</text>
<text x="29.718" y="-63.5" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT23/AIN1) PD7</text>
<text x="29.718" y="-60.96" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT22/OC0A/AIN0) PD6</text>
<text x="29.718" y="-58.42" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT21/OPC0/T1) PD5</text>
<text x="29.718" y="-55.88" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT20/XCX/T0) PD4</text>
<text x="29.718" y="-53.34" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT19/OC2B/INT1) PD3</text>
<text x="29.718" y="-50.8" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT18/INT0) PD2</text>
<text x="29.718" y="-48.26" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT17/TXD) PD1</text>
<text x="29.718" y="-45.72" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT16/RXD) PD0</text>
<pin name="PC6" x="33.02" y="-40.64" visible="off" length="short" rot="R180"/>
<text x="29.718" y="-40.64" size="1.016" layer="97" font="vector" rot="R180" align="center-left">(PCINT14/RESET) PC6</text>
<wire x1="0" y1="0" x2="0" y2="-66.04" width="0.254" layer="94"/>
<wire x1="0" y1="-66.04" x2="30.48" y2="-66.04" width="0.254" layer="94"/>
<wire x1="30.48" y1="-66.04" x2="30.48" y2="0" width="0.254" layer="94"/>
<wire x1="30.48" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328P" prefix="U">
<description>&lt;b&gt;Manufacturer Part #:&lt;/b&gt; ATMEGA328P&lt;br/&gt;
&lt;b&gt;Manufacturer:&lt;/b&gt; Atmel&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; 8-bit Microcontrollers - MCU 32KB In-system Flash 20MHz 1.8V-5.5V&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="ATMEGA328P" x="0" y="0"/>
</gates>
<devices>
<device name="-AU" package="TQFP32">
<connects>
<connect gate="G$1" pin="ADC6" pad="P$19"/>
<connect gate="G$1" pin="ADC7" pad="P$22"/>
<connect gate="G$1" pin="AREF" pad="P$20"/>
<connect gate="G$1" pin="AVCC" pad="P$18"/>
<connect gate="G$1" pin="GND" pad="P$3 P$5 P$21"/>
<connect gate="G$1" pin="PB0" pad="P$12"/>
<connect gate="G$1" pin="PB1" pad="P$13"/>
<connect gate="G$1" pin="PB2" pad="P$14"/>
<connect gate="G$1" pin="PB3" pad="P$15"/>
<connect gate="G$1" pin="PB4" pad="P$16"/>
<connect gate="G$1" pin="PB5" pad="P$17"/>
<connect gate="G$1" pin="PB6" pad="P$7"/>
<connect gate="G$1" pin="PB7" pad="P$8"/>
<connect gate="G$1" pin="PC0" pad="P$23"/>
<connect gate="G$1" pin="PC1" pad="P$24"/>
<connect gate="G$1" pin="PC2" pad="P$25"/>
<connect gate="G$1" pin="PC3" pad="P$26"/>
<connect gate="G$1" pin="PC4" pad="P$27"/>
<connect gate="G$1" pin="PC5" pad="P$28"/>
<connect gate="G$1" pin="PC6" pad="P$29"/>
<connect gate="G$1" pin="PD0" pad="P$30"/>
<connect gate="G$1" pin="PD1" pad="P$31"/>
<connect gate="G$1" pin="PD2" pad="P$32"/>
<connect gate="G$1" pin="PD3" pad="P$1"/>
<connect gate="G$1" pin="PD4" pad="P$2"/>
<connect gate="G$1" pin="PD5" pad="P$9"/>
<connect gate="G$1" pin="PD6" pad="P$10"/>
<connect gate="G$1" pin="PD7" pad="P$11"/>
<connect gate="G$1" pin="VCC" pad="P$4 P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="ATMEGA328P-AU" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="http://www.atmel.com/devices/atmega328p.aspx" constant="no"/>
<attribute name="VALUE" value="ATMEGA328P-AU" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Connectors">
<packages>
<package name="PTH_2.54MM_02X03">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for 2.54MM Pitch Header 2 Row 6 Position. Based on 4UCON 00989.&lt;br&gt;</description>
<pad name="P$1" x="-2.54" y="-1.27" drill="1"/>
<wire x1="-3.79" y1="2.52" x2="3.79" y2="2.52" width="0.127" layer="21"/>
<wire x1="3.79" y1="2.52" x2="3.79" y2="-2.52" width="0.127" layer="21"/>
<wire x1="3.79" y1="-2.52" x2="-3.79" y2="-2.52" width="0.127" layer="21"/>
<wire x1="-3.79" y1="-2.52" x2="-3.79" y2="2.52" width="0.127" layer="21"/>
<text x="-5.056" y="-2.52" size="1.016" layer="25" font="vector" ratio="16" rot="R90">&gt;NAME</text>
<pad name="P$2" x="-2.54" y="1.27" drill="1"/>
<pad name="P$3" x="0" y="-1.27" drill="1"/>
<text x="-2.54" y="2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-right">&gt;LABEL02</text>
<text x="-2.54" y="-2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL01</text>
<polygon width="0.127" layer="21">
<vertex x="-4.445" y="-3.175"/>
<vertex x="-3.81" y="-3.175"/>
<vertex x="-3.175" y="-2.54"/>
<vertex x="-3.81" y="-1.905"/>
<vertex x="-4.445" y="-2.54"/>
</polygon>
<pad name="P$4" x="0" y="1.27" drill="1"/>
<pad name="P$5" x="2.54" y="-1.27" drill="1"/>
<pad name="P$6" x="2.54" y="1.27" drill="1"/>
<text x="0" y="-2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL03</text>
<text x="0" y="2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-right">&gt;LABEL04</text>
<text x="2.54" y="-2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL05</text>
<text x="2.54" y="2.794" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-right">&gt;LABEL06</text>
</package>
<package name="TC2030-IDC-FP">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Tag Connect TC2030-FP style&lt;br&gt;
&lt;b&gt;MacroFab House Part:&lt;/b&gt; No &lt;br&gt;
&lt;b&gt;URL:&lt;/b&gt; http://www.tag-connect.com/TC2030-IDC&lt;br&gt;</description>
<smd name="P$1" x="-1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$2" x="-1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$3" x="0" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$4" x="0" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$5" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$6" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<hole x="2.54" y="1.016" drill="0.9906"/>
<hole x="2.54" y="-1.016" drill="0.9906"/>
<hole x="-2.54" y="0" drill="0.9906"/>
<wire x1="3.683" y1="-3.048" x2="-3.683" y2="-3.048" width="0.127" layer="21"/>
<wire x1="-3.683" y1="-3.048" x2="-4.953" y2="-1.778" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.953" y1="-1.778" x2="-4.953" y2="1.778" width="0.127" layer="21"/>
<wire x1="-4.953" y1="1.778" x2="-3.683" y2="3.048" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.683" y1="3.048" x2="3.683" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.683" y1="3.048" x2="4.953" y2="1.778" width="0.127" layer="21" curve="-90"/>
<wire x1="4.953" y1="1.778" x2="4.953" y2="-1.778" width="0.127" layer="21"/>
<wire x1="4.953" y1="-1.778" x2="3.683" y2="-3.048" width="0.127" layer="21" curve="-90"/>
<text x="-1.27" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL01</text>
<text x="-1.27" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL02</text>
<text x="0" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL03</text>
<text x="0" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL04</text>
<text x="1.27" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL05</text>
<text x="1.27" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL06</text>
<text x="-5.715" y="0" size="1.016" layer="25" font="vector" ratio="16" rot="R90" align="center">&gt;NAME</text>
<hole x="-2.54" y="2.54" drill="2.375"/>
<hole x="0.635" y="2.54" drill="2.375"/>
<hole x="0.635" y="-2.54" drill="2.375"/>
<hole x="-2.54" y="-2.54" drill="2.375"/>
</package>
<package name="TC2030-IDC-NL">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Tag Connect TC2030-NL style&lt;br&gt;
&lt;b&gt;MacroFab House Part:&lt;/b&gt; No &lt;br&gt;
&lt;b&gt;URL:&lt;/b&gt; http://www.tag-connect.com/TC2030-IDC-NL&lt;br&gt;</description>
<smd name="P$1" x="-1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$2" x="-1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$3" x="0" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$4" x="0" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$5" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="P$6" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<hole x="2.54" y="1.016" drill="0.9906"/>
<hole x="2.54" y="-1.016" drill="0.9906"/>
<hole x="-2.54" y="0" drill="0.9906"/>
<wire x1="3.683" y1="-3.048" x2="-3.683" y2="-3.048" width="0.127" layer="21"/>
<wire x1="-3.683" y1="-3.048" x2="-4.953" y2="-1.778" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.953" y1="-1.778" x2="-4.953" y2="1.778" width="0.127" layer="21"/>
<wire x1="-4.953" y1="1.778" x2="-3.683" y2="3.048" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.683" y1="3.048" x2="3.683" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.683" y1="3.048" x2="4.953" y2="1.778" width="0.127" layer="21" curve="-90"/>
<wire x1="4.953" y1="1.778" x2="4.953" y2="-1.778" width="0.127" layer="21"/>
<wire x1="4.953" y1="-1.778" x2="3.683" y2="-3.048" width="0.127" layer="21" curve="-90"/>
<text x="-1.27" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL01</text>
<text x="-1.27" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL02</text>
<text x="0" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL03</text>
<text x="0" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL04</text>
<text x="1.27" y="-3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL05</text>
<text x="1.27" y="3.81" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-left">&gt;LABEL06</text>
<text x="-5.715" y="0" size="1.016" layer="25" font="vector" ratio="16" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="PTH_2.54MM_01X08">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for 2.54MM Pitch Header 8 Position. Based on 4UCON 00834.&lt;br&gt;</description>
<pad name="P$1" x="-8.89" y="0" drill="1"/>
<wire x1="-10.14" y1="1.25" x2="10.14" y2="1.25" width="0.127" layer="21"/>
<wire x1="10.14" y1="1.25" x2="10.14" y2="-1.25" width="0.127" layer="21"/>
<wire x1="10.14" y1="-1.25" x2="-10.14" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-10.14" y1="-1.25" x2="-10.14" y2="1.25" width="0.127" layer="21"/>
<text x="-10.14" y="1.5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<pad name="P$2" x="-6.35" y="0" drill="1"/>
<pad name="P$3" x="-3.81" y="0" drill="1"/>
<pad name="P$4" x="-1.27" y="0" drill="1"/>
<pad name="P$5" x="1.27" y="0" drill="1"/>
<pad name="P$6" x="3.81" y="0" drill="1"/>
<pad name="P$7" x="6.35" y="0" drill="1"/>
<pad name="P$8" x="8.89" y="0" drill="1"/>
<text x="6.35" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL07</text>
<text x="3.81" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL06</text>
<text x="1.27" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL05</text>
<text x="-1.27" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL04</text>
<text x="-3.81" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL03</text>
<text x="-6.35" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL02</text>
<text x="-8.89" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL01</text>
<polygon width="0.127" layer="21">
<vertex x="-10.795" y="-1.905"/>
<vertex x="-10.16" y="-1.905"/>
<vertex x="-9.525" y="-1.27"/>
<vertex x="-10.16" y="-0.635"/>
<vertex x="-10.795" y="-1.27"/>
</polygon>
<text x="8.89" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL08</text>
</package>
<package name="PTH_2.54MM_01X06">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for 2.54MM Pitch Header 6 Position. Based on 4UCON 00834.&lt;br&gt;</description>
<pad name="P$1" x="-6.35" y="0" drill="1"/>
<wire x1="-7.6" y1="1.25" x2="7.6" y2="1.25" width="0.127" layer="21"/>
<wire x1="7.6" y1="1.25" x2="7.6" y2="-1.25" width="0.127" layer="21"/>
<wire x1="7.6" y1="-1.25" x2="-7.6" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-7.6" y1="-1.25" x2="-7.6" y2="1.25" width="0.127" layer="21"/>
<text x="-7.6" y="1.5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<pad name="P$2" x="-3.81" y="0" drill="1"/>
<pad name="P$3" x="-1.27" y="0" drill="1"/>
<pad name="P$4" x="1.27" y="0" drill="1"/>
<pad name="P$5" x="3.81" y="0" drill="1"/>
<pad name="P$6" x="6.35" y="0" drill="1"/>
<text x="3.81" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL05</text>
<text x="1.27" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL04</text>
<text x="-1.27" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL03</text>
<text x="-3.81" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL02</text>
<text x="-6.35" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R90" align="center-right">&gt;LABEL01</text>
<polygon width="0.127" layer="21">
<vertex x="-8.255" y="-1.905"/>
<vertex x="-7.62" y="-1.905"/>
<vertex x="-6.985" y="-1.27"/>
<vertex x="-7.62" y="-0.635"/>
<vertex x="-8.255" y="-1.27"/>
</polygon>
<text x="6.35" y="-1.27" size="1.016" layer="21" font="vector" ratio="16" rot="R270" align="center-left">&gt;LABEL06</text>
</package>
</packages>
<symbols>
<symbol name="CON_02X03">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Connector 6 Position 2 Row.&lt;br&gt;</description>
<pin name="PIN1" x="-2.54" y="0" visible="off" length="short"/>
<text x="7.62" y="0" size="1.016" layer="97" font="vector" rot="MR180" align="center-left">&gt;LABEL01</text>
<wire x1="0" y1="-6.35" x2="30.48" y2="-6.35" width="0.127" layer="94"/>
<pin name="PIN6" x="33.02" y="-5.08" visible="off" length="short" rot="R180"/>
<wire x1="0" y1="1.27" x2="0" y2="-6.35" width="0.127" layer="94"/>
<wire x1="30.48" y1="-6.35" x2="30.48" y2="1.27" width="0.127" layer="94"/>
<wire x1="30.48" y1="1.27" x2="0" y2="1.27" width="0.127" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="PIN2" x="33.02" y="0" visible="off" length="short" rot="R180"/>
<text x="22.86" y="0" size="1.016" layer="97" font="vector" rot="MR0" align="center-left">&gt;LABEL02</text>
<text x="0.508" y="0" size="1.016" layer="97" font="vector" rot="MR180" align="center-left">PIN01</text>
<text x="29.972" y="0" size="1.016" layer="97" font="vector" rot="MR0" align="center-left">PIN02</text>
<pin name="PIN3" x="-2.54" y="-2.54" visible="off" length="short"/>
<text x="7.62" y="-2.54" size="1.016" layer="97" font="vector" rot="MR180" align="center-left">&gt;LABEL03</text>
<text x="0.508" y="-2.54" size="1.016" layer="97" font="vector" rot="MR0" align="center-right">PIN03</text>
<pin name="PIN4" x="33.02" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="PIN5" x="-2.54" y="-5.08" visible="off" length="short"/>
<text x="0.508" y="-5.08" size="1.016" layer="97" font="vector" rot="MR0" align="center-right">PIN05</text>
<text x="22.86" y="-2.54" size="1.016" layer="97" font="vector" rot="MR0" align="center-left">&gt;LABEL04</text>
<text x="7.62" y="-5.08" size="1.016" layer="97" font="vector" rot="MR180" align="center-left">&gt;LABEL05</text>
<text x="29.972" y="-2.54" size="1.016" layer="97" font="vector" rot="MR180" align="center-right">PIN04</text>
<text x="29.972" y="-5.08" size="1.016" layer="97" font="vector" rot="MR180" align="center-right">PIN06</text>
<text x="22.86" y="-5.08" size="1.016" layer="97" font="vector" rot="MR0" align="center-left">&gt;LABEL06</text>
</symbol>
<symbol name="CON_01X08">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Connector 8 Position 1 Row.&lt;br&gt;</description>
<pin name="PIN1" x="-2.54" y="0" visible="off" length="short"/>
<wire x1="0" y1="-19.05" x2="16.51" y2="-19.05" width="0.127" layer="94"/>
<pin name="PIN8" x="-2.54" y="-17.78" visible="off" length="short"/>
<wire x1="0" y1="1.27" x2="0" y2="-19.05" width="0.127" layer="94"/>
<wire x1="16.51" y1="-19.05" x2="16.51" y2="1.27" width="0.127" layer="94"/>
<wire x1="16.51" y1="1.27" x2="0" y2="1.27" width="0.127" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="PIN2" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="PIN3" x="-2.54" y="-5.08" visible="off" length="short"/>
<pin name="PIN4" x="-2.54" y="-7.62" visible="off" length="short"/>
<pin name="PIN5" x="-2.54" y="-10.16" visible="off" length="short"/>
<pin name="PIN6" x="-2.54" y="-12.7" visible="off" length="short"/>
<pin name="PIN7" x="-2.54" y="-15.24" visible="off" length="short"/>
<text x="7.62" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL05</text>
<text x="7.62" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL04</text>
<text x="7.62" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL03</text>
<text x="7.62" y="0" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL01</text>
<text x="7.62" y="-17.78" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL08</text>
<text x="7.62" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL02</text>
<text x="0.508" y="-17.78" size="1.016" layer="97" font="vector" align="center-left">PIN08</text>
<text x="0.508" y="0" size="1.016" layer="97" font="vector" align="center-left">PIN01</text>
<text x="0.508" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">PIN02</text>
<text x="0.508" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">PIN03</text>
<text x="0.508" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">PIN04</text>
<text x="0.508" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">PIN05</text>
<text x="0.508" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">PIN06</text>
<text x="7.62" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL06</text>
<text x="7.62" y="-15.24" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL07</text>
<text x="0.508" y="-15.24" size="1.016" layer="97" font="vector" align="center-left">PIN07</text>
</symbol>
<symbol name="CON_01X06">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Connector 6 Position 1 Row.&lt;br&gt;</description>
<pin name="PIN1" x="-2.54" y="0" visible="off" length="short"/>
<wire x1="0" y1="-13.97" x2="16.51" y2="-13.97" width="0.127" layer="94"/>
<pin name="PIN6" x="-2.54" y="-12.7" visible="off" length="short"/>
<wire x1="0" y1="1.27" x2="0" y2="-13.97" width="0.127" layer="94"/>
<wire x1="16.51" y1="-13.97" x2="16.51" y2="1.27" width="0.127" layer="94"/>
<wire x1="16.51" y1="1.27" x2="0" y2="1.27" width="0.127" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="PIN2" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="PIN3" x="-2.54" y="-5.08" visible="off" length="short"/>
<pin name="PIN4" x="-2.54" y="-7.62" visible="off" length="short"/>
<pin name="PIN5" x="-2.54" y="-10.16" visible="off" length="short"/>
<text x="7.62" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL05</text>
<text x="7.62" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL04</text>
<text x="7.62" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL03</text>
<text x="7.62" y="0" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL01</text>
<text x="7.62" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL06</text>
<text x="7.62" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">&gt;LABEL02</text>
<text x="0.508" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">PIN06</text>
<text x="0.508" y="0" size="1.016" layer="97" font="vector" align="center-left">PIN01</text>
<text x="0.508" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">PIN02</text>
<text x="0.508" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">PIN03</text>
<text x="0.508" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">PIN04</text>
<text x="0.508" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">PIN05</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CON_02X03" prefix="TC">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Header Connector with 6 Positions and 02 Rows. Use the LABEL Attribute to label the pin on the schematic and PCB. You can remove the attributes on the PCB by using the SMASH function on the part and then deleting the text.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CON_02X03" x="0" y="0"/>
</gates>
<devices>
<device name="_PTH_2.54MM" package="PTH_2.54MM_02X03">
<connects>
<connect gate="G$1" pin="PIN1" pad="P$1"/>
<connect gate="G$1" pin="PIN2" pad="P$2"/>
<connect gate="G$1" pin="PIN3" pad="P$3"/>
<connect gate="G$1" pin="PIN4" pad="P$4"/>
<connect gate="G$1" pin="PIN5" pad="P$5"/>
<connect gate="G$1" pin="PIN6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="LABEL01" value="" constant="no"/>
<attribute name="LABEL02" value="" constant="no"/>
<attribute name="LABEL03" value="" constant="no"/>
<attribute name="LABEL04" value="" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="MF-CON-2.54mm-2x3" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-CON-2.54mm-2x3 " constant="no"/>
<attribute name="VALUE" value="MF-CON-2.54mm-2x3" constant="no"/>
</technology>
</technologies>
</device>
<device name="_TC2030-IDC" package="TC2030-IDC-FP">
<connects>
<connect gate="G$1" pin="PIN1" pad="P$1"/>
<connect gate="G$1" pin="PIN2" pad="P$2"/>
<connect gate="G$1" pin="PIN3" pad="P$3"/>
<connect gate="G$1" pin="PIN4" pad="P$4"/>
<connect gate="G$1" pin="PIN5" pad="P$5"/>
<connect gate="G$1" pin="PIN6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="" constant="no"/>
<attribute name="LABEL02" value="" constant="no"/>
<attribute name="LABEL03" value="" constant="no"/>
<attribute name="LABEL04" value="" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="TC2030-IDC" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-IDC" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC" constant="no"/>
</technology>
<technology name="_AVRISP">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="MISO" constant="no"/>
<attribute name="LABEL02" value="VCC" constant="no"/>
<attribute name="LABEL03" value="SCK" constant="no"/>
<attribute name="LABEL04" value="MOSI" constant="no"/>
<attribute name="LABEL05" value="RST" constant="no"/>
<attribute name="LABEL06" value="GND" constant="no"/>
<attribute name="MPN" value="TC2030-IDC" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/AVRISP" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC" constant="no"/>
</technology>
<technology name="_FTDI-TTL-232R">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="VCC" constant="no"/>
<attribute name="LABEL02" value="CTS/TMS" constant="no"/>
<attribute name="LABEL03" value="RX/TDI" constant="no"/>
<attribute name="LABEL04" value="TX/TCLK" constant="no"/>
<attribute name="LABEL05" value="GND" constant="no"/>
<attribute name="LABEL06" value="RTS/TDO" constant="no"/>
<attribute name="MPN" value="TC2030-FTDI-TTL-232R " constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-FTDI" constant="no"/>
<attribute name="VALUE" value="TC2030-FTDI-TTL-232R " constant="no"/>
</technology>
<technology name="_PICKIT">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="!MCLR" constant="no"/>
<attribute name="LABEL02" value="VDD Target" constant="no"/>
<attribute name="LABEL03" value="GND" constant="no"/>
<attribute name="LABEL04" value="PGD" constant="no"/>
<attribute name="LABEL05" value="PGC" constant="no"/>
<attribute name="LABEL06" value="PGM" constant="no"/>
<attribute name="MPN" value="TC2030-PKT" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-PKT" constant="no"/>
<attribute name="VALUE" value="TC2030-PKT" constant="no"/>
</technology>
<technology name="_SILABS2TC">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="GND" constant="no"/>
<attribute name="LABEL02" value="VCC" constant="no"/>
<attribute name="LABEL03" value="C2CK" constant="no"/>
<attribute name="LABEL04" value="C2D" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="TC2030-IDC" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-IDC" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC" constant="no"/>
</technology>
</technologies>
</device>
<device name="_TC2030-IDC-NL" package="TC2030-IDC-NL">
<connects>
<connect gate="G$1" pin="PIN1" pad="P$1"/>
<connect gate="G$1" pin="PIN2" pad="P$2"/>
<connect gate="G$1" pin="PIN3" pad="P$3"/>
<connect gate="G$1" pin="PIN4" pad="P$4"/>
<connect gate="G$1" pin="PIN5" pad="P$5"/>
<connect gate="G$1" pin="PIN6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="" constant="no"/>
<attribute name="LABEL02" value="" constant="no"/>
<attribute name="LABEL03" value="" constant="no"/>
<attribute name="LABEL04" value="" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="TC2030-IDC-NL" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-IDC-NL" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC-NL" constant="no"/>
</technology>
<technology name="_AVRISP">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="MISO" constant="no"/>
<attribute name="LABEL02" value="VCC" constant="no"/>
<attribute name="LABEL03" value="SCK" constant="no"/>
<attribute name="LABEL04" value="MOSI" constant="no"/>
<attribute name="LABEL05" value="RST" constant="no"/>
<attribute name="LABEL06" value="GND" constant="no"/>
<attribute name="MPN" value="TC2030-IDC-NL" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/AVRISP" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC-NL" constant="no"/>
</technology>
<technology name="_FTDI-TTL-232R">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="VCC" constant="no"/>
<attribute name="LABEL02" value="CTS/TMS " constant="no"/>
<attribute name="LABEL03" value="RX/TDI " constant="no"/>
<attribute name="LABEL04" value="TX/TCLK " constant="no"/>
<attribute name="LABEL05" value="GND" constant="no"/>
<attribute name="LABEL06" value="RTS/TDO " constant="no"/>
<attribute name="MPN" value="TC2030-FTDI-TTL-232R-NL " constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-FTDI" constant="no"/>
<attribute name="VALUE" value="TC2030-FTDI-TTL-232R-NL " constant="no"/>
</technology>
<technology name="_PICKIT">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="!MCLR" constant="no"/>
<attribute name="LABEL02" value="VDD Target " constant="no"/>
<attribute name="LABEL03" value="GND" constant="no"/>
<attribute name="LABEL04" value="PGD" constant="no"/>
<attribute name="LABEL05" value="PGC" constant="no"/>
<attribute name="LABEL06" value="PGM" constant="no"/>
<attribute name="MPN" value="TC2030-PKT-NL " constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-PKT-NL" constant="no"/>
<attribute name="VALUE" value="TC2030-PKT-NL " constant="no"/>
</technology>
<technology name="_SILABS2TC">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="LABEL01" value="GND" constant="no"/>
<attribute name="LABEL02" value="VCC" constant="no"/>
<attribute name="LABEL03" value="C2CK" constant="no"/>
<attribute name="LABEL04" value="C2D" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="TC2030-IDC-NL" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="http://www.tag-connect.com/TC2030-IDC-NL" constant="no"/>
<attribute name="VALUE" value="TC2030-IDC-NL" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON_01X08" prefix="J">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Header Connector with 8 Positions and 01 Rows. Use the LABEL Attribute to label the pin on the schematic and PCB. You can remove the attributes on the PCB by using the SMASH function on the part and then deleting the text.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CON_01X08" x="0" y="0"/>
</gates>
<devices>
<device name="_PTH_2.54MM" package="PTH_2.54MM_01X08">
<connects>
<connect gate="G$1" pin="PIN1" pad="P$1"/>
<connect gate="G$1" pin="PIN2" pad="P$2"/>
<connect gate="G$1" pin="PIN3" pad="P$3"/>
<connect gate="G$1" pin="PIN4" pad="P$4"/>
<connect gate="G$1" pin="PIN5" pad="P$5"/>
<connect gate="G$1" pin="PIN6" pad="P$6"/>
<connect gate="G$1" pin="PIN7" pad="P$7"/>
<connect gate="G$1" pin="PIN8" pad="P$8"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="LABEL01" value="" constant="no"/>
<attribute name="LABEL02" value="" constant="no"/>
<attribute name="LABEL03" value="" constant="no"/>
<attribute name="LABEL04" value="" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="LABEL07" value="" constant="no"/>
<attribute name="LABEL08" value="" constant="no"/>
<attribute name="MPN" value="MF-CON-2.54mm-01x08" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-CON-2.54mm-01x08" constant="no"/>
<attribute name="VALUE" value="MF-CON-2.54mm-01x08" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON_01X06" prefix="J">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Header Connector with 6 Positions and 01 Rows. Use the LABEL Attribute to label the pin on the schematic and PCB. You can remove the attributes on the PCB by using the SMASH function on the part and then deleting the text.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CON_01X06" x="0" y="0"/>
</gates>
<devices>
<device name="_PTH_2.54MM" package="PTH_2.54MM_01X06">
<connects>
<connect gate="G$1" pin="PIN1" pad="P$1"/>
<connect gate="G$1" pin="PIN2" pad="P$2"/>
<connect gate="G$1" pin="PIN3" pad="P$3"/>
<connect gate="G$1" pin="PIN4" pad="P$4"/>
<connect gate="G$1" pin="PIN5" pad="P$5"/>
<connect gate="G$1" pin="PIN6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="LABEL01" value="" constant="no"/>
<attribute name="LABEL02" value="" constant="no"/>
<attribute name="LABEL03" value="" constant="no"/>
<attribute name="LABEL04" value="" constant="no"/>
<attribute name="LABEL05" value="" constant="no"/>
<attribute name="LABEL06" value="" constant="no"/>
<attribute name="MPN" value="MF-CON-2.54mm-01x06" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-CON-2.54mm-01x06" constant="no"/>
<attribute name="VALUE" value="MF-CON-2.54mm-01x06" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Passives">
<packages>
<package name="R0402">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0402 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<smd name="P$2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<wire x1="-1.1" y1="0.55" x2="-1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.55" x2="1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.55" x2="1.1" y2="0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.55" x2="-1.1" y2="0.55" width="0.127" layer="21"/>
<text x="-1.1" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0603 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<smd name="P$2" x="0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="1.4" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.7" x2="-1.4" y2="0.7" width="0.127" layer="21"/>
<text x="-1.4" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0805 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<smd name="P$2" x="0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<wire x1="-1.8" y1="0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<text x="-1.8" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R1206">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1206 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.1" x2="-2.2" y2="1.1" width="0.127" layer="21"/>
<text x="-2.2" y="1.3" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="R1210">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1210 Package for Resistors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.6" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.6" x2="2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.6" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<text x="-2.2" y="1.8" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0402">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0402 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<smd name="P$2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" rot="R180"/>
<wire x1="-1.1" y1="0.55" x2="-1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.55" x2="1.1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.55" x2="1.1" y2="0.55" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.55" x2="-1.1" y2="0.55" width="0.127" layer="21"/>
<text x="-1.1" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0603 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<smd name="P$2" x="0.75" y="0" dx="0.6" dy="0.9" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="1.4" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="1.4" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.7" x2="-1.4" y2="0.7" width="0.127" layer="21"/>
<text x="-1.4" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 0805 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<smd name="P$2" x="0.95" y="0" dx="0.7" dy="1.3" layer="1" rot="R180"/>
<wire x1="-1.8" y1="0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<text x="-1.8" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C1206">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1206 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="1.6" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.1" x2="-2.2" y2="1.1" width="0.127" layer="21"/>
<text x="-2.2" y="1.3" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="C1210">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 1210 Package for Capacitors&lt;br/&gt;</description>
<smd name="P$1" x="-1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<smd name="P$2" x="1.45" y="0" dx="0.9" dy="2.5" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.6" x2="-2.2" y2="-1.575" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.575" x2="2.2" y2="-1.575" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.575" x2="2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.6" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<text x="-2.2" y="1.8" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="CP4MM">
<description>&lt;b&gt;Description:&lt;/b&gt; 4MM Round Package for Electrolytic Capacitors. Based on Panasonic Electrolytic Capacitors.&lt;br&gt;</description>
<smd name="P$2" x="0" y="1.75" dx="2.5" dy="1.6" layer="1" rot="R90"/>
<smd name="P$1" x="0" y="-1.75" dx="2.5" dy="1.6" layer="1" rot="R90"/>
<wire x1="-1.2" y1="2.2" x2="-2.2" y2="2.2" width="0.127" layer="21"/>
<wire x1="-2.2" y1="2.2" x2="-2.2" y2="-1.56360625" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.56360625" x2="-1.56360625" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-1.56360625" y1="-2.2" x2="-1.1" y2="-2.2" width="0.127" layer="21"/>
<wire x1="1.2" y1="2.2" x2="2.2" y2="2.2" width="0.127" layer="21"/>
<wire x1="2.2" y1="2.2" x2="2.2" y2="-1.56360625" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.56360625" x2="1.56360625" y2="-2.2" width="0.127" layer="21"/>
<wire x1="1.56360625" y1="-2.2" x2="1.2" y2="-2.2" width="0.127" layer="21"/>
<text x="-2.2" y="3.5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="21"/>
</package>
<package name="CP6.3MM">
<description>&lt;b&gt;Description:&lt;/b&gt; 6.3MM Round Package for Electrolytic Capacitors. Based on Panasonic Electrolytic Capacitors.&lt;br&gt;</description>
<smd name="P$1" x="0" y="-2.7" dx="3.2" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="2.7" dx="3.2" dy="1.6" layer="1" rot="R90"/>
<wire x1="1.1" y1="3.3" x2="3.3" y2="3.3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3.3" x2="3.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-2.3" x2="2.3" y2="-3.3" width="0.127" layer="21"/>
<wire x1="2.3" y1="-3.3" x2="1.1" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-1.1" y1="3.3" x2="-3.3" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-2.3" x2="-2.3" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-3.3" x2="-1.1" y2="-3.3" width="0.127" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<text x="-3.3" y="5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="CP8MM">
<description>&lt;b&gt;Description:&lt;/b&gt; 8MM Round Package for Electrolytic Capacitors. Based on Panasonic Electrolytic Capacitors.&lt;br&gt;</description>
<smd name="P$1" x="0" y="-3.1" dx="4" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="3.1" dx="4" dy="1.6" layer="1" rot="R90"/>
<wire x1="-1.5" y1="4.15" x2="-4.15" y2="4.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="4.15" x2="-4.15" y2="-2.88" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-2.88" x2="-2.88" y2="-4.15" width="0.127" layer="21"/>
<wire x1="-2.88" y1="-4.15" x2="-1.5" y2="-4.15" width="0.127" layer="21"/>
<wire x1="1.5" y1="-4.15" x2="2.88" y2="-4.15" width="0.127" layer="21"/>
<wire x1="2.88" y1="-4.15" x2="4.15" y2="-2.88" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2.88" x2="4.15" y2="4.15" width="0.127" layer="21"/>
<wire x1="4.15" y1="4.15" x2="1.5" y2="4.15" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<text x="-4.15" y="6" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="CP10MM">
<description>&lt;b&gt;Description:&lt;/b&gt; 10MM Round Package for Electrolytic Capacitors. Based on Panasonic Electrolytic Capacitors.
&lt;br/&gt;</description>
<smd name="P$1" x="0" y="-4.35" dx="4.1" dy="2" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="4.35" dx="4.1" dy="2" layer="1" rot="R90"/>
<text x="-5.15" y="7.5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<wire x1="-1.5" y1="5.15" x2="-5.15" y2="5.15" width="0.127" layer="21"/>
<wire x1="-5.15" y1="5.15" x2="-5.15" y2="-3.15" width="0.127" layer="21"/>
<wire x1="-5.15" y1="-3.15" x2="-3.15" y2="-5.15" width="0.127" layer="21"/>
<wire x1="-3.15" y1="-5.15" x2="-1.5" y2="-5.15" width="0.127" layer="21"/>
<wire x1="1.5" y1="5.15" x2="5.15" y2="5.15" width="0.127" layer="21"/>
<wire x1="5.15" y1="5.15" x2="5.15" y2="-3.15" width="0.127" layer="21"/>
<wire x1="5.15" y1="-3.15" x2="3.15" y2="-5.15" width="0.127" layer="21"/>
<wire x1="3.15" y1="-5.15" x2="1.5" y2="-5.15" width="0.127" layer="21"/>
<wire x1="3" y1="-2.5" x2="3" y2="-3.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-3" x2="3.5" y2="-3" width="0.127" layer="21"/>
</package>
<package name="CP12.5MM">
<description>&lt;b&gt;Description:&lt;/b&gt; 12.5MM Round Package for Electrolytic Capacitors. Based on Panasonic Electrolytic Capacitors.&lt;br&gt;</description>
<smd name="P$1" x="0" y="-4.85" dx="5.7" dy="2" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="4.85" dx="5.7" dy="2" layer="1" rot="R90"/>
<wire x1="-1.75" y1="6.75" x2="-6.75" y2="6.75" width="0.127" layer="21"/>
<wire x1="-6.75" y1="6.75" x2="-6.75" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-4.25" x2="-4.25" y2="-6.75" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-6.75" x2="-1.75" y2="-6.75" width="0.127" layer="21"/>
<wire x1="1.75" y1="6.75" x2="6.75" y2="6.75" width="0.127" layer="21"/>
<wire x1="6.75" y1="6.75" x2="6.75" y2="-4.25" width="0.127" layer="21"/>
<wire x1="6.75" y1="-4.25" x2="4.25" y2="-6.75" width="0.127" layer="21"/>
<wire x1="4.25" y1="-6.75" x2="1.75" y2="-6.75" width="0.127" layer="21"/>
<wire x1="4" y1="-3.5" x2="4" y2="-5.5" width="0.127" layer="21"/>
<wire x1="5" y1="-4.5" x2="3" y2="-4.5" width="0.127" layer="21"/>
<text x="-6.75" y="9" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Symbol for Resistors&lt;br/&gt;</description>
<pin name="P$1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="1.016" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.016" y1="2.159" x2="-1.016" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="1.016" y2="0.889" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.889" x2="-1.016" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.254" x2="1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.381" x2="-1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.651" x2="-1.016" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.286" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="1.524" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR_NP">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Symbol for Non-Polarized Capacitors&lt;br/&gt;</description>
<pin name="P$1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P$2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="2.54" y="1.524" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<wire x1="0" y1="2.54" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<rectangle x1="-1.778" y1="0.508" x2="1.778" y2="1.27" layer="94"/>
<rectangle x1="-1.778" y1="-1.27" x2="1.778" y2="-0.508" layer="94"/>
</symbol>
<symbol name="CAPACITOR_P">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Symbol for Polarized Capacitors&lt;br/&gt;</description>
<pin name="P$1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P$2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.524" x2="2.286" y2="-1.524" width="0.1524" layer="94" curve="-100"/>
<wire x1="-2.286" y1="0.762" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="2.286" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.286" x2="1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.778" y1="1.778" x2="0.762" y2="1.778" width="0.1524" layer="94"/>
<text x="2.54" y="1.524" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Resistors. Manufacture part number (MPN), Voltage, Tolerance, and Wattage Rating can be added via Attributes.  Check https://factory.macrofab.com/parts for the house parts list.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="R0402">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603" package="R0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="R0805">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="R1206">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1210" package="R1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
<attribute name="WATTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_NP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Non-Polarized Capacitors. Manufacture part number (MPN) can be added via Attributes. Check https://factory.macrofab.com/parts for the house parts list.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CAPACITOR_NP" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="C0402">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603" package="C0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="C0805">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="C1206">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1210" package="C1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIELECTRIC" value="" constant="no"/>
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_P" prefix="C" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Passives&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Polarized Capacitors. Manufacture part number (MPN), Voltage Rating, and Tolerance can be added via Attributes. Check https://factory.macrofab.com/parts for the house parts list.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CAPACITOR_P" x="0" y="0"/>
</gates>
<devices>
<device name="_4MM" package="CP4MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_6.3MM" package="CP6.3MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOTLAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_8MM" package="CP8MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_10MM" package="CP10MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_12.5MM" package="CP12.5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="TOLERANCE" value="" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Aesthetics">
<packages>
</packages>
<symbols>
<symbol name="POWER">
<description>&lt;b&gt;Description: &lt;/b &gt;Symbol for power rails and indication.&lt;br/&gt;</description>
<pin name="P$1" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.54" size="1.016" layer="96" font="vector" align="center">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="GND">
<description>&lt;b&gt;Description: &lt;/b &gt;Symbol for ground rails and indication.&lt;br/&gt;</description>
<pin name="P$1" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-2.54" size="1.016" layer="96" font="vector" rot="R180" align="center">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-1.27" y="0"/>
<vertex x="0" y="-1.27"/>
<vertex x="1.27" y="0"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="POWER_RAIL" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Aesthetics&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt;Symbol for adding an indicator to a power rail.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="POWER" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND_RAIL" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Aesthetics&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt;Symbol for adding an indicator to a ground rail.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="0" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Frequency_Control">
<packages>
<package name="7MMX5MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for 7MM x 5MM Crystals.&lt;br/&gt;</description>
<smd name="P$1" x="-1.27" y="3.15" dx="2.2" dy="1.4" layer="1" rot="R270"/>
<smd name="P$2" x="1.27" y="3.15" dx="2.2" dy="1.4" layer="1" rot="R270"/>
<smd name="P$3" x="1.27" y="-3.15" dx="2.2" dy="1.4" layer="1" rot="R270"/>
<smd name="P$4" x="-1.27" y="-3.05" dx="2.4" dy="1.4" layer="1" rot="R270"/>
<wire x1="2.6" y1="3.6" x2="2.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="2.6" y1="-3.6" x2="2.2" y2="-3.6" width="0.127" layer="21"/>
<wire x1="2.6" y1="3.6" x2="2.2" y2="3.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="3.6" x2="-2.6" y2="3.6" width="0.127" layer="21"/>
<wire x1="-2.6" y1="3.6" x2="-2.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-3.6" x2="-2.2" y2="-3.6" width="0.127" layer="21"/>
<wire x1="0.2" y1="-3.6" x2="-0.2" y2="-3.6" width="0.127" layer="21"/>
<wire x1="0.2" y1="3.6" x2="-0.2" y2="3.6" width="0.127" layer="21"/>
<text x="-2.6" y="4.572" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="5MMX3.2MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for 5MM x 3.2MM Crystals.&lt;br/&gt;</description>
<smd name="P$1" x="-1.1" y="1.85" dx="2" dy="1.2" layer="1" rot="R270"/>
<smd name="P$2" x="-1.1" y="-1.85" dx="2" dy="1.2" layer="1" rot="R270"/>
<smd name="P$3" x="1.1" y="-1.85" dx="2" dy="1.2" layer="1" rot="R270"/>
<smd name="P$4" x="1.1" y="1.85" dx="2" dy="1.2" layer="1" rot="R270"/>
<wire x1="1.6" y1="0.6" x2="1.6" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.2" y1="-2.5" x2="-0.2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0.2" y1="2.5" x2="-0.2" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.6" x2="-1.6" y2="-0.6" width="0.127" layer="21"/>
<text x="-1.6" y="3.175" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL_GND">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Crystals that need a ground.&lt;br/&gt;</description>
<pin name="X0" x="-5.08" y="0" visible="off" length="short"/>
<pin name="X1" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="GND" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.524" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.524" x2="2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-1.524" x2="2.032" y2="1.524" width="0.1524" layer="94"/>
<wire x1="2.032" y1="1.524" x2="-2.032" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-3.048" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-2.286" x2="0" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.286" x2="3.048" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.286" x2="3.048" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.286" width="0.1524" layer="94"/>
<text x="-5.08" y="5.08" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-5.08" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CYRSTAL_GND" prefix="X" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Frequency_Control&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for two functional pin Cyrstal Oscillators and a ground. &lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL_GND" x="0" y="0"/>
</gates>
<devices>
<device name="_ABMM" package="7MMX5MM">
<connects>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
<connect gate="G$1" pin="X0" pad="P$1"/>
<connect gate="G$1" pin="X1" pad="P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="http://www.abracon.com/Resonators/ABMM.pdf" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_ABM3B" package="5MMX3.2MM">
<connects>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
<connect gate="G$1" pin="X0" pad="P$1"/>
<connect gate="G$1" pin="X1" pad="P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="http://www.abracon.com/Resonators/abm3b.pdf" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Switches">
<packages>
<package name="TACT6MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 6MM Tact Switch Package.&lt;br/&gt;</description>
<smd name="P$1" x="-3.975" y="2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$2" x="3.975" y="2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$3" x="-3.975" y="-2.45" dx="1.55" dy="1.3" layer="1"/>
<smd name="P$4" x="3.975" y="-2.45" dx="1.55" dy="1.3" layer="1"/>
<wire x1="-5.2" y1="3.4" x2="-5.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.2" y1="-3.4" x2="5.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="5.2" y1="-3.4" x2="5.2" y2="3.4" width="0.127" layer="21"/>
<wire x1="5.2" y1="3.4" x2="-5.2" y2="3.4" width="0.127" layer="21"/>
<wire x1="-2.67" y1="2.4" x2="0" y2="2.4" width="0.127" layer="21"/>
<wire x1="0" y1="2.4" x2="2.67" y2="2.4" width="0.127" layer="21"/>
<wire x1="-2.67" y1="-2.4" x2="0" y2="-2.4" width="0.127" layer="21"/>
<wire x1="0" y1="-2.4" x2="2.67" y2="-2.4" width="0.127" layer="21"/>
<wire x1="0" y1="2.4" x2="0" y2="1" width="0.127" layer="21"/>
<wire x1="0" y1="-2.4" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="-0.8" y2="0.8" width="0.127" layer="21"/>
<text x="-5.2" y="3.6" size="1.016" layer="21" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="TACT4.2MM">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard 4.2MM Tact Switch Package.Based off C&amp;K PTS 810 Series Tact Switch.&lt;br/&gt;</description>
<smd name="P$3" x="-2.075" y="-1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$4" x="2.075" y="-1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$2" x="2.075" y="1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<smd name="P$1" x="-2.075" y="1.075" dx="0.65" dy="1.05" layer="1" rot="R90"/>
<wire x1="-2.8" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.8" y1="1.6" x2="2.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.8" y1="-1.6" x2="-2.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="-2.8" y2="1.6" width="0.127" layer="21"/>
<text x="-2.8" y="2" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<wire x1="-1.2" y1="1.2" x2="0" y2="1.2" width="0.127" layer="25"/>
<wire x1="0" y1="1.2" x2="1.2" y2="1.2" width="0.127" layer="25"/>
<wire x1="-1.2" y1="-1.2" x2="0" y2="-1.2" width="0.127" layer="25"/>
<wire x1="0" y1="-1.2" x2="1.2" y2="-1.2" width="0.127" layer="25"/>
<wire x1="0" y1="-1.2" x2="0" y2="-0.6" width="0.127" layer="25"/>
<wire x1="0" y1="1.2" x2="0" y2="0.6" width="0.127" layer="25"/>
<wire x1="0" y1="-0.6" x2="-0.4" y2="0.4" width="0.127" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="TACTSW">
<description>&lt;b&gt;Description:&lt;/b&gt; Tact Switch Symbol.&lt;br/&gt;</description>
<pin name="P$1" x="-2.54" y="2.54" visible="off" length="short"/>
<pin name="P$2" x="2.54" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="P$3" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="P$4" x="2.54" y="-2.54" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="7.62" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.016" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TACT" prefix="SW" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Switches&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Tact Switches. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="TACTSW" x="0" y="0"/>
</gates>
<devices>
<device name="_6MM" package="TACT6MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-SW-TACT-6MM" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-SW-TACT-6MM" constant="no"/>
<attribute name="VALUE" value="MF-SW-TACT-6MM" constant="no"/>
</technology>
</technologies>
</device>
<device name="_4.2MM" package="TACT4.2MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-SW-TACT-4.2MM" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-SW-TACT-4.2MM" constant="no"/>
<attribute name="VALUE" value="MF-SW-TACT-4.2MM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_LEDs">
<packages>
<package name="LED0603">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for Single LEDs in 0603&lt;br/&gt;</description>
<smd name="CATHODE" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="ANODE" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.5" y="0.75"/>
<vertex x="-1.7" y="0.75"/>
<vertex x="-1.7" y="-0.75"/>
<vertex x="-1.5" y="-0.75"/>
</polygon>
<text x="-1.7" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.15" y="0"/>
<vertex x="0.15" y="0.3"/>
<vertex x="0.15" y="-0.3"/>
</polygon>
</package>
<package name="LED0805">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for Single LEDs in 0805&lt;br/&gt;</description>
<smd name="CATHODE" x="-1.25" y="0" dx="1.25" dy="1.1" layer="1" rot="R90"/>
<smd name="ANODE" x="1.25" y="0" dx="1.25" dy="1.1" layer="1" rot="R90"/>
<wire x1="-2.05" y1="0.85" x2="-2.05" y2="-0.85" width="0.127" layer="21"/>
<wire x1="-2.05" y1="-0.85" x2="2.05" y2="-0.85" width="0.127" layer="21"/>
<wire x1="2.05" y1="-0.85" x2="2.05" y2="0.85" width="0.127" layer="21"/>
<wire x1="2.05" y1="0.85" x2="-2.05" y2="0.85" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.25" y="0.85"/>
<vertex x="-2.25" y="-0.85"/>
<vertex x="-2.05" y="-0.85"/>
</polygon>
<text x="-2.25" y="1.1" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.15" y="0"/>
<vertex x="0.15" y="0.3"/>
<vertex x="0.15" y="-0.3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for Single LEDs&lt;br/&gt;</description>
<wire x1="1.27" y1="1.524" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="1.016" y2="0" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-0.508" y="-1.524"/>
<vertex x="-0.508" y="1.524"/>
<vertex x="1.016" y="0"/>
</polygon>
<wire x1="-0.508" y1="2.286" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.826" x2="0.508" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.508" y1="4.826" x2="1.016" y2="4.318" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.318" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.286" x2="1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.778" y1="3.302" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.826" x2="1.778" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.778" y1="4.826" x2="2.286" y2="4.318" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.318" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short" direction="pwr"/>
<pin name="CATHODE" x="2.54" y="0" visible="off" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_SINGLE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_LEDs&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Single LED Packages. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0603-RED" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0603-RED" constant="no"/>
<attribute name="VALUE" value="MF-LED-0603-RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-RED" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0805-RED" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0805-RED" constant="no"/>
<attribute name="VALUE" value="MF-LED-0805-RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0603-GREEN" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0603-GREEN" constant="no"/>
<attribute name="VALUE" value="MF-LED-0603-GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-GREEN" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-0805-GREEN" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-0805-GREEN" constant="no"/>
<attribute name="VALUE" value="MF-LED-0805-GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="LED0603">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="LED0805">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Texas Instruments - DRV8835DSSR">
<description>Upverter Parts Library

Created by Upverter.com</description>
<packages>
<package name="TEXAS_INSTRUMENTS_DRV8835DSSR_0">
<description>Dual Low-Voltage H-Bridge IC</description>
<wire x1="-1" y1="-1.5" x2="-1" y2="1.5" width="0.15" layer="51"/>
<wire x1="-1" y1="1.5" x2="1" y2="1.5" width="0.15" layer="51"/>
<wire x1="1" y1="1.5" x2="1" y2="-1.5" width="0.15" layer="51"/>
<wire x1="1" y1="-1.5" x2="-1" y2="-1.5" width="0.15" layer="51"/>
<wire x1="-1.3" y1="-1.65" x2="-1.3" y2="1.65" width="0.1" layer="39"/>
<wire x1="-1.3" y1="1.65" x2="1.3" y2="1.65" width="0.1" layer="39"/>
<wire x1="1.3" y1="1.65" x2="1.3" y2="-1.65" width="0.1" layer="39"/>
<wire x1="1.3" y1="-1.65" x2="-1.3" y2="-1.65" width="0.1" layer="39"/>
<wire x1="-1" y1="1.775" x2="1" y2="1.775" width="0.15" layer="21"/>
<wire x1="-1" y1="-1.775" x2="1" y2="-1.775" width="0.15" layer="21"/>
<text x="-1.5" y="2.275" size="1" layer="25">&gt;NAME</text>
<circle x="-1.775" y="1.25" radius="0.25" width="0" layer="21"/>
<pad name="PAD_1" x="0" y="0.75" drill="0.2" diameter="0.5"/>
<pad name="PAD_2" x="0" y="0" drill="0.2" diameter="0.5"/>
<pad name="PAD_3" x="0" y="-0.75" drill="0.2" diameter="0.5"/>
<smd name="1" x="-0.95" y="1.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="2" x="-0.95" y="0.75" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="3" x="-0.95" y="0.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="4" x="-0.95" y="-0.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="5" x="-0.95" y="-0.75" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="6" x="-0.95" y="-1.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="7" x="0.95" y="-1.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="8" x="0.95" y="-0.75" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="9" x="0.95" y="-0.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="10" x="0.95" y="0.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="11" x="0.95" y="0.75" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="12" x="0.95" y="1.25" dx="0.5" dy="0.25" layer="1" roundness="40"/>
<smd name="13" x="0" y="0" dx="0.9" dy="2" layer="1" roundness="11"/>
<smd name="SMD_14" x="0" y="0.55" dx="0.9" dy="0.9" layer="1" roundness="11"/>
<smd name="SMD_15" x="0" y="-0.55" dx="0.9" dy="0.9" layer="1" roundness="11"/>
</package>
</packages>
<symbols>
<symbol name="TEXAS_INSTRUMENTS_DRV8835DSSR_0_0">
<description>Dual Low-Voltage H-Bridge IC</description>
<wire x1="2.54" y1="-33.02" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="45.72" y2="-5.08" width="0.254" layer="94"/>
<wire x1="45.72" y1="-5.08" x2="45.72" y2="-33.02" width="0.254" layer="94"/>
<wire x1="45.72" y1="-33.02" x2="2.54" y2="-33.02" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-7.62" width="0.15" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="-7.62" width="0.15" layer="94"/>
<wire x1="45.72" y1="-10.16" x2="45.72" y2="-10.16" width="0.15" layer="94"/>
<wire x1="45.72" y1="-15.24" x2="45.72" y2="-15.24" width="0.15" layer="94"/>
<wire x1="45.72" y1="-17.78" x2="45.72" y2="-17.78" width="0.15" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="2.54" y2="-27.94" width="0.15" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="2.54" y2="-30.48" width="0.15" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="-15.24" width="0.15" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-10.16" width="0.15" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="2.54" y2="-20.32" width="0.15" layer="94"/>
<wire x1="2.54" y1="-22.86" x2="2.54" y2="-22.86" width="0.15" layer="94"/>
<wire x1="45.72" y1="-27.94" x2="45.72" y2="-27.94" width="0.15" layer="94"/>
<wire x1="45.72" y1="-30.48" x2="45.72" y2="-30.48" width="0.15" layer="94"/>
<text x="2.54" y="-2.54" size="2.54" layer="95" align="top-left">&gt;NAME</text>
<text x="2.54" y="-35.56" size="2.54" layer="95" align="top-left">DRV8835DSSR</text>
<pin name="VCC" x="-2.54" y="-7.62" length="middle"/>
<pin name="AOUT1" x="50.8" y="-7.62" length="middle" rot="R180"/>
<pin name="AOUT2" x="50.8" y="-10.16" length="middle" rot="R180"/>
<pin name="BOUT1" x="50.8" y="-15.24" length="middle" rot="R180"/>
<pin name="BOUT2" x="50.8" y="-17.78" length="middle" rot="R180"/>
<pin name="BIN1/BPHASE" x="-2.54" y="-27.94" length="middle"/>
<pin name="BIN2/BENBL" x="-2.54" y="-30.48" length="middle"/>
<pin name="MODE" x="-2.54" y="-15.24" length="middle"/>
<pin name="VM" x="-2.54" y="-10.16" length="middle"/>
<pin name="AIN1/APHASE" x="-2.54" y="-20.32" length="middle"/>
<pin name="AIN2/AENBL" x="-2.54" y="-22.86" length="middle"/>
<pin name="GND" x="50.8" y="-27.94" length="middle" rot="R180"/>
<pin name="GND_PAD" x="50.8" y="-30.48" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEXAS_INSTRUMENTS_DRV8835DSSR" prefix="U">
<description>Dual Low-Voltage H-Bridge IC</description>
<gates>
<gate name="G$0" symbol="TEXAS_INSTRUMENTS_DRV8835DSSR_0_0" x="0" y="0"/>
</gates>
<devices>
<device name="TEXAS_INSTRUMENTS_DRV8835DSSR_0_0" package="TEXAS_INSTRUMENTS_DRV8835DSSR_0">
<connects>
<connect gate="G$0" pin="AIN1/APHASE" pad="10"/>
<connect gate="G$0" pin="AIN2/AENBL" pad="9"/>
<connect gate="G$0" pin="AOUT1" pad="2"/>
<connect gate="G$0" pin="AOUT2" pad="3"/>
<connect gate="G$0" pin="BIN1/BPHASE" pad="8"/>
<connect gate="G$0" pin="BIN2/BENBL" pad="7"/>
<connect gate="G$0" pin="BOUT1" pad="4"/>
<connect gate="G$0" pin="BOUT2" pad="5"/>
<connect gate="G$0" pin="GND" pad="6"/>
<connect gate="G$0" pin="GND_PAD" pad="13"/>
<connect gate="G$0" pin="MODE" pad="11"/>
<connect gate="G$0" pin="VCC" pad="12"/>
<connect gate="G$0" pin="VM" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CENTROID_NOT_SPECIFIED" value="No"/>
<attribute name="DATASHEET" value="http://www.ti.com/lit/ds/symlink/drv8835.pdf"/>
<attribute name="DIGIKEY_DESCRIPTION" value="IC MOTOR DRIVER PAR 12WSON"/>
<attribute name="DIGIKEY_PART_NUMBER" value="296-30391-1-ND"/>
<attribute name="LEAD_FREE" value="yes"/>
<attribute name="MF" value="Texas Instruments"/>
<attribute name="MFG_PACKAGE_IDENT" value="DSS0012A"/>
<attribute name="MOUSER_PART_NUMBER" value="595-DRV8835DSSR"/>
<attribute name="MPN" value="DRV8835DSSR"/>
<attribute name="PACKAGE" value="WSON12"/>
<attribute name="PREFIX" value="U"/>
<attribute name="ROHS" value="yes"/>
<attribute name="TEMPERATURE_RANGE_HIGH" value="+85°C"/>
<attribute name="TEMPERATURE_RANGE_LOW" value="-40°C"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X06-SMD_RA_MALE" urn="urn:adsk.eagle:footprint:37588/1" library_version="1">
<description>&lt;h3&gt;SMD - 6 Pin Right Angle Male Header&lt;/h3&gt;
tDocu layer shows pin locations.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
<text x="-1.524" y="0.381" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X06" urn="urn:adsk.eagle:footprint:37589/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 6 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX_1X6_RA_LOCK" urn="urn:adsk.eagle:footprint:37590/1" library_version="1">
<description>&lt;h3&gt;Molex 6-Pin Plated Through-Hole Right Angle Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering.  
tPlace shows location of connector.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="22"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="4.826" y="5.588" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="4.699" y="4.318" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X06_LONGPADS" urn="urn:adsk.eagle:footprint:37591/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 6 Pin with Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06_LOCK" urn="urn:adsk.eagle:footprint:37592/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 6 Pin with Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, locking pins in place during soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X06-KIT" urn="urn:adsk.eagle:footprint:37593/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 6 Pin KIT&lt;/h3&gt;
&lt;p&gt;This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. 
&lt;br&gt;This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="51"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="51"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="51"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="51"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="51"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="51"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="51"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="51"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="51"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="51"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="51"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="51"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<polygon width="0.127" layer="30">
<vertex x="0.0025" y="-0.9525" curve="-90"/>
<vertex x="-0.9524" y="-0.0228" curve="-90.011749"/>
<vertex x="0" y="0.9526" curve="-90"/>
<vertex x="0.95" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0" y="-0.4445" curve="-90.012891"/>
<vertex x="-0.4445" y="-0.0203" curve="-90"/>
<vertex x="0" y="0.447" curve="-90"/>
<vertex x="0.4419" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="2.5425" y="-0.9525" curve="-90"/>
<vertex x="1.5876" y="-0.0228" curve="-90.011749"/>
<vertex x="2.54" y="0.9526" curve="-90"/>
<vertex x="3.49" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="2.54" y="-0.4445" curve="-90.012891"/>
<vertex x="2.0955" y="-0.0203" curve="-90"/>
<vertex x="2.54" y="0.447" curve="-90"/>
<vertex x="2.9819" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="5.0825" y="-0.9525" curve="-90"/>
<vertex x="4.1276" y="-0.0228" curve="-90.011749"/>
<vertex x="5.08" y="0.9526" curve="-90"/>
<vertex x="6.03" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="5.08" y="-0.4445" curve="-90.012891"/>
<vertex x="4.6355" y="-0.0203" curve="-90"/>
<vertex x="5.08" y="0.447" curve="-90"/>
<vertex x="5.5219" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="7.6225" y="-0.9525" curve="-90"/>
<vertex x="6.6676" y="-0.0228" curve="-90.011749"/>
<vertex x="7.62" y="0.9526" curve="-90"/>
<vertex x="8.57" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="7.62" y="-0.4445" curve="-90.012891"/>
<vertex x="7.1755" y="-0.0203" curve="-90"/>
<vertex x="7.62" y="0.447" curve="-90"/>
<vertex x="8.0619" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="10.1625" y="-0.9525" curve="-90"/>
<vertex x="9.2076" y="-0.0228" curve="-90.011749"/>
<vertex x="10.16" y="0.9526" curve="-90"/>
<vertex x="11.11" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="10.16" y="-0.4445" curve="-90.012891"/>
<vertex x="9.7155" y="-0.0203" curve="-90"/>
<vertex x="10.16" y="0.447" curve="-90"/>
<vertex x="10.6019" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="12.7025" y="-0.9525" curve="-90"/>
<vertex x="11.7476" y="-0.0228" curve="-90.011749"/>
<vertex x="12.7" y="0.9526" curve="-90"/>
<vertex x="13.65" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="12.7" y="-0.4445" curve="-90.012891"/>
<vertex x="12.2555" y="-0.0203" curve="-90"/>
<vertex x="12.7" y="0.447" curve="-90"/>
<vertex x="13.1419" y="-0.0101" curve="-90.012967"/>
</polygon>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="6_PIN_SERIAL_TARGET_SIDE_W_SILK" urn="urn:adsk.eagle:footprint:37594/1" library_version="1">
<description>&lt;h3&gt;6 pin Serial Target - Right Angle  SMT with Silk&lt;/h3&gt;
Package for devices meant to mate to an FTDI connector.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;6_Pin_Serial_Target&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P$1" x="-6.35" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$2" x="-3.81" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$3" x="-1.27" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$4" x="1.27" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$5" x="3.81" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$6" x="6.35" y="0" drill="1.016" diameter="1.8796"/>
<wire x1="-7.62" y1="1.27" x2="7.62" y2="1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<text x="-7.874" y="-0.889" size="1.27" layer="21" font="vector" ratio="15" rot="R90">GRN</text>
<text x="9.144" y="-0.889" size="1.27" layer="21" font="vector" ratio="15" rot="R90">BLK</text>
<text x="-4.826" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">TXO</text>
<text x="-2.286" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">RXI</text>
<text x="0.254" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">VCC</text>
<text x="-7.366" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">DTR</text>
<text x="5.334" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">GND</text>
<text x="2.794" y="1.524" size="0.8128" layer="21" font="vector" ratio="15">CTS</text>
</package>
<package name="6_PIN_SERIAL_TARGET_SIDE_RA_SMT" urn="urn:adsk.eagle:footprint:37595/1" library_version="1">
<description>&lt;h3&gt;6 pin Serial Target - Right Angle  SMT&lt;/h3&gt;
Package for devices meant to mate to an FTDI connector. 
&lt;p&gt; tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;6_Pin_Serial_Target&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<text x="-3.81" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">TXO</text>
<text x="-1.27" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">RXI</text>
<text x="1.27" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">VCC</text>
<text x="-6.35" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">DTR</text>
<text x="6.35" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">GND</text>
<text x="3.81" y="-1.016" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="center-left">CTS</text>
<text x="-7.366" y="-2.54" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="top-center">GRN</text>
<text x="7.366" y="-2.54" size="0.8128" layer="51" font="vector" ratio="15" rot="R90" align="bottom-center">BLK</text>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
</package>
<package name="1X02" urn="urn:adsk.eagle:footprint:37654/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2" urn="urn:adsk.eagle:footprint:37655/1" library_version="1">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2" urn="urn:adsk.eagle:footprint:37656/1" library_version="1">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD" urn="urn:adsk.eagle:footprint:37657/1" library_version="1">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG" urn="urn:adsk.eagle:footprint:37658/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT" urn="urn:adsk.eagle:footprint:37659/1" library_version="1">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2" urn="urn:adsk.eagle:footprint:37660/1" library_version="1">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK" urn="urn:adsk.eagle:footprint:37661/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK" urn="urn:adsk.eagle:footprint:37662/1" library_version="1">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS" urn="urn:adsk.eagle:footprint:37663/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK" urn="urn:adsk.eagle:footprint:37664/1" library_version="1">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS" urn="urn:adsk.eagle:footprint:37665/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK" urn="urn:adsk.eagle:footprint:37666/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH" urn="urn:adsk.eagle:footprint:37667/1" library_version="1">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG" urn="urn:adsk.eagle:footprint:37668/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY" urn="urn:adsk.eagle:footprint:37669/1" library_version="1">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS" urn="urn:adsk.eagle:footprint:37670/1" library_version="1">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS" urn="urn:adsk.eagle:footprint:37671/1" library_version="1">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT" urn="urn:adsk.eagle:footprint:37672/1" library_version="1">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2" urn="urn:adsk.eagle:footprint:37673/1" library_version="1">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM" urn="urn:adsk.eagle:footprint:37674/1" library_version="1">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_POKEHOME" urn="urn:adsk.eagle:footprint:37675/1" library_version="1">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_RA_PTH_FEMALE" urn="urn:adsk.eagle:footprint:37676/1" library_version="1">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
<package name="1X01_LONGPAD" urn="urn:adsk.eagle:footprint:37641/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - Long Pad&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01" urn="urn:adsk.eagle:footprint:37642/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_2MM" urn="urn:adsk.eagle:footprint:37643/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="2" diameter="3.302" rot="R90"/>
<text x="-1.651" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01_OFFSET" urn="urn:adsk.eagle:footprint:37644/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - Long Pad w/ Offset Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="offset" rot="R90"/>
<text x="-1.27" y="3.048" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01_POGOPIN_HOLE_0.061_DIA" urn="urn:adsk.eagle:footprint:37645/1" library_version="1">
<description>&lt;h3&gt;Pogo Pin - 0.061"&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.061"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.5494"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01_POGOPIN_HOLE_0.58_DIA" urn="urn:adsk.eagle:footprint:37646/1" library_version="1">
<description>&lt;h3&gt;Pogo Pin Hole - 0.58" &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.58"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90" thermals="no"/>
<hole x="0" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SNAP-FEMALE" urn="urn:adsk.eagle:footprint:37647/1" library_version="1">
<description>&lt;h3&gt;Sew-On Fabric Snap - Female&lt;/h3&gt;
Equivalent to size #1/0 snap. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:8mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="2.921" diameter="4.572"/>
<polygon width="0.254" layer="1">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="29">
<vertex x="-4.0005" y="0" curve="-90.002865"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="31">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<polygon width="0.3556" layer="41">
<vertex x="-4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="4.0005" curve="-90.002865"/>
<vertex x="4.0005" y="0" curve="-89.997136"/>
<vertex x="0" y="-4.0005" curve="-89.997136"/>
</polygon>
<text x="-1.27" y="4.318" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.953" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SNAP-MALE" urn="urn:adsk.eagle:footprint:37648/1" library_version="1">
<description>&lt;h3&gt;Sew-On Fabric Snap - Male&lt;/h3&gt;
Equivalent to size #1/0 snap. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:8mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="2" x="0" y="0" dx="7.62" dy="7.62" layer="1" roundness="100"/>
<text x="-1.524" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-4.826" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SPRING-CONNECTOR" urn="urn:adsk.eagle:footprint:37649/1" library_version="1">
<description>&lt;h3&gt;Spring Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;li&gt;Area:0.25"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="P$2" x="0" y="0" dx="7.112" dy="7.112" layer="1" roundness="100"/>
<text x="-1.27" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X01NS_KIT" urn="urn:adsk.eagle:footprint:37650/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline Kit Version&lt;/h3&gt;
&lt;p&gt; Mask on only one side to make soldering in kits easier.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9398" width="0" layer="30"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X01_NO_SILK" urn="urn:adsk.eagle:footprint:37651/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:1&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="SMTSO-256-ET-0.165DIA" urn="urn:adsk.eagle:footprint:37652/1" library_version="1">
<description>&lt;h3&gt;SMTSO-256-ET Flush Mount Nut&lt;/h3&gt;
.165 drill
&lt;br&gt;
Fits 4-40 Screws. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_01&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.286" x2="-2.286" y2="1.016" width="1.016" layer="31" curve="42.075022"/>
<wire x1="2.286" y1="1.016" x2="1.016" y2="2.286" width="1.016" layer="31" curve="42.075022"/>
<wire x1="1.016" y1="-2.286" x2="2.286" y2="-1.016" width="1.016" layer="31" curve="42.075022"/>
<wire x1="-2.286" y1="-1.016" x2="-1.016" y2="-2.286" width="1.016" layer="31" curve="42.075022"/>
<pad name="P$1" x="0" y="0" drill="4.191" diameter="6.1976"/>
<text x="-1.397" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-3.937" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="1X06-SMD_RA_MALE" urn="urn:adsk.eagle:package:38004/1" type="box" library_version="1">
<description>SMD - 6 Pin Right Angle Male Header
tDocu layer shows pin locations.
Specifications:
Pin count:6
Pin pitch:0.1"

Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="1X06-SMD_RA_MALE"/>
</packageinstances>
</package3d>
<package3d name="1X06" urn="urn:adsk.eagle:package:38009/1" type="box" library_version="1">
<description>Plated Through Hole - 6 Pin
Specifications:
Pin count:6
Pin pitch:0.1"

Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="1X06"/>
</packageinstances>
</package3d>
<package3d name="MOLEX_1X6_RA_LOCK" urn="urn:adsk.eagle:package:37992/1" type="box" library_version="1">
<description>Molex 6-Pin Plated Through-Hole Right Angle Locking Footprint
Holes are offset 0.005" from center to hold pins in place during soldering.  
tPlace shows location of connector.
Specifications:
Pin count:6
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="MOLEX_1X6_RA_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X06_LONGPADS" urn="urn:adsk.eagle:package:37991/1" type="box" library_version="1">
<description>Plated Through Hole - 6 Pin with Long Pads
Specifications:
Pin count:6
Pin pitch:0.1"

Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="1X06_LONGPADS"/>
</packageinstances>
</package3d>
<package3d name="1X06_LOCK" urn="urn:adsk.eagle:package:38002/1" type="box" library_version="1">
<description>Plated Through Hole - 6 Pin with Locking Footprint
Holes are offset 0.005" from center, locking pins in place during soldering.
Specifications:
Pin count:6
Pin pitch:0.1"

Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="1X06_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X06-KIT" urn="urn:adsk.eagle:package:38006/1" type="box" library_version="1">
<description>Plated Through Hole - 6 Pin KIT
This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. 
This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
Specifications:
Pin count:6
Pin pitch:0.1"

Example device(s):
CONN_06
</description>
<packageinstances>
<packageinstance name="1X06-KIT"/>
</packageinstances>
</package3d>
<package3d name="6_PIN_SERIAL_TARGET_SIDE_W_SILK" urn="urn:adsk.eagle:package:37995/1" type="box" library_version="1">
<description>6 pin Serial Target - Right Angle  SMT with Silk
Package for devices meant to mate to an FTDI connector.
Specifications:
Pin count:6
Pin pitch: 0.1"

Example device(s):
6_Pin_Serial_Target
</description>
<packageinstances>
<packageinstance name="6_PIN_SERIAL_TARGET_SIDE_W_SILK"/>
</packageinstances>
</package3d>
<package3d name="6_PIN_SERIAL_TARGET_SIDE_RA_SMT" urn="urn:adsk.eagle:package:38001/1" type="box" library_version="1">
<description>6 pin Serial Target - Right Angle  SMT
Package for devices meant to mate to an FTDI connector. 
 tDocu shows pin location. 
Specifications:
Pin count:6
Pin pitch: 0.1"

Example device(s):
6_Pin_Serial_Target
</description>
<packageinstances>
<packageinstance name="6_PIN_SERIAL_TARGET_SIDE_RA_SMT"/>
</packageinstances>
</package3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:38039/1" type="box" library_version="1">
<description>Plated Through Hole
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-1X2" urn="urn:adsk.eagle:package:38040/1" type="box" library_version="1">
<description>Molex 2-Pin Plated Through-Hole
Specifications:
Pin count:2
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="MOLEX-1X2"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2" urn="urn:adsk.eagle:package:38050/1" type="box" library_version="1">
<description>Screw Terminal  3.5mm Pitch - 2 Pin PTH
Specifications:
Pin count: 2
Pin pitch: 3.5mm/138mil

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2"/>
</packageinstances>
</package3d>
<package3d name="JST-2-SMD" urn="urn:adsk.eagle:package:38042/1" type="box" library_version="1">
<description>JST-Right Angle Male Header SMT
Specifications:
Pin count: 2
Pin pitch: 2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
JST_2MM_MALE
</description>
<packageinstances>
<packageinstance name="JST-2-SMD"/>
</packageinstances>
</package3d>
<package3d name="1X02_BIG" urn="urn:adsk.eagle:package:38043/1" type="box" library_version="1">
<description>Plated Through Hole
Specifications:
Pin count:2
Pin pitch:0.15"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_BIG"/>
</packageinstances>
</package3d>
<package3d name="JST-2-SMD-VERT" urn="urn:adsk.eagle:package:38052/1" type="box" library_version="1">
<description>JST-Vertical Male Header SMT 
Specifications:
Pin count: 2
Pin pitch: 2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="JST-2-SMD-VERT"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-5MM-2" urn="urn:adsk.eagle:package:38044/1" type="box" library_version="1">
<description>Screw Terminal  5mm Pitch -2 Pin PTH
Specifications:
Pin count: 2
Pin pitch: 5mm/197mil

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="SCREWTERMINAL-5MM-2"/>
</packageinstances>
</package3d>
<package3d name="1X02_LOCK" urn="urn:adsk.eagle:package:38045/1" type="box" library_version="1">
<description>Plated Through Hole - Locking Footprint
Holes are staggered by 0.005" from center to hold pins while soldering. 
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_LOCK"/>
</packageinstances>
</package3d>
<package3d name="MOLEX-1X2_LOCK" urn="urn:adsk.eagle:package:38046/1" type="box" library_version="1">
<description>Molex 2-Pin Plated Through-Hole Locking Footprint
Holes are offset from center by 0.005" to hold pins in place during soldering. 
Specifications:
Pin count:2
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="MOLEX-1X2_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X02_LOCK_LONGPADS" urn="urn:adsk.eagle:package:38047/1" type="box" library_version="1">
<description>Plated Through Hole - Long Pads with Locking Footprint
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_LOCK_LONGPADS"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2_LOCK" urn="urn:adsk.eagle:package:38049/1" type="box" library_version="1">
<description>Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking
Holes are offset from center 0.005" to hold pins in place during soldering. 
Specifications:
Pin count: 2
Pin pitch: 3.5mm/138mil

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2_LOCK"/>
</packageinstances>
</package3d>
<package3d name="1X02_LONGPADS" urn="urn:adsk.eagle:package:38048/1" type="box" library_version="1">
<description>Plated Through Hole - Long Pads without Silk Outline
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_LONGPADS"/>
</packageinstances>
</package3d>
<package3d name="1X02_NO_SILK" urn="urn:adsk.eagle:package:38051/1" type="box" library_version="1">
<description>Plated Through Hole - No Silk Outline
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_NO_SILK"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH" urn="urn:adsk.eagle:package:38053/1" type="box" library_version="1">
<description>JST 2 Pin Right Angle Plated Through  Hole
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
Specifications:
Pin count: 2
Pin pitch:2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="JST-2-PTH"/>
</packageinstances>
</package3d>
<package3d name="1X02_XTRA_BIG" urn="urn:adsk.eagle:package:38054/1" type="box" library_version="1">
<description>Plated Through Hole - 0.1" holes
Specifications:
Pin count:2
Pin pitch:0.2"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_XTRA_BIG"/>
</packageinstances>
</package3d>
<package3d name="1X02_PP_HOLES_ONLY" urn="urn:adsk.eagle:package:38058/1" type="box" library_version="1">
<description>Pogo Pins Connector - No Silk Outline
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_PP_HOLES_ONLY"/>
</packageinstances>
</package3d>
<package3d name="SCREWTERMINAL-3.5MM-2-NS" urn="urn:adsk.eagle:package:38055/1" type="box" library_version="1">
<description>Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline
Specifications:
Pin count: 2
Pin pitch: 3.5mm/138mil

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="SCREWTERMINAL-3.5MM-2-NS"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH-NS" urn="urn:adsk.eagle:package:38056/1" type="box" library_version="1">
<description>JST 2 Pin Right Angle Plated Through  Hole- No Silk
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
 No silk outline of connector. 
Specifications:
Pin count: 2
Pin pitch:2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="JST-2-PTH-NS"/>
</packageinstances>
</package3d>
<package3d name="JST-2-PTH-KIT" urn="urn:adsk.eagle:package:38057/1" type="box" library_version="1">
<description>JST 2 Pin Right Angle Plated Through  Hole - KIT
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
 This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
 This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
Specifications:
Pin count: 2
Pin pitch:2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="JST-2-PTH-KIT"/>
</packageinstances>
</package3d>
<package3d name="SPRINGTERMINAL-2.54MM-2" urn="urn:adsk.eagle:package:38061/1" type="box" library_version="1">
<description>Spring Terminal- PCB Mount 2 Pin PTH
tDocu marks the spring arms
Specifications:
Pin count: 4
Pin pitch: 0.1"

Datasheet referenced for footprint
Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="SPRINGTERMINAL-2.54MM-2"/>
</packageinstances>
</package3d>
<package3d name="1X02_2.54_SCREWTERM" urn="urn:adsk.eagle:package:38059/1" type="box" library_version="1">
<description>2 Pin Screw Terminal - 2.54mm
Specifications:
Pin count:2
Pin pitch:0.1"

Example device(s):
CONN_02
</description>
<packageinstances>
<packageinstance name="1X02_2.54_SCREWTERM"/>
</packageinstances>
</package3d>
<package3d name="1X02_POKEHOME" urn="urn:adsk.eagle:package:38060/1" type="box" library_version="1">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<packageinstances>
<packageinstance name="1X02_POKEHOME"/>
</packageinstances>
</package3d>
<package3d name="1X02_RA_PTH_FEMALE" urn="urn:adsk.eagle:package:38062/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="1X02_RA_PTH_FEMALE"/>
</packageinstances>
</package3d>
<package3d name="1X01_LONGPAD" urn="urn:adsk.eagle:package:38030/1" type="box" library_version="1">
<description>Plated Through Hole - Long Pad
Specifications:
Pin count:1
Pin pitch:0.1"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_LONGPAD"/>
</packageinstances>
</package3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:38028/1" type="box" library_version="1">
<description>Plated Through Hole
Specifications:
Pin count:1
Pin pitch:0.1"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01"/>
</packageinstances>
</package3d>
<package3d name="1X01_2MM" urn="urn:adsk.eagle:package:38029/1" type="box" library_version="1">
<description>Plated Through Hole - 2mm
Specifications:
Pin count:1

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_2MM"/>
</packageinstances>
</package3d>
<package3d name="1X01_OFFSET" urn="urn:adsk.eagle:package:38035/1" type="box" library_version="1">
<description>Plated Through Hole - Long Pad w/ Offset Hole
Specifications:
Pin count:1

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_OFFSET"/>
</packageinstances>
</package3d>
<package3d name="1X01_POGOPIN_HOLE_0.061_DIA" urn="urn:adsk.eagle:package:38036/1" type="box" library_version="1">
<description>Pogo Pin - 0.061"
Specifications:
Pin count:1
Pin pitch:0.061"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_POGOPIN_HOLE_0.061_DIA"/>
</packageinstances>
</package3d>
<package3d name="1X01_POGOPIN_HOLE_0.58_DIA" urn="urn:adsk.eagle:package:38031/1" type="box" library_version="1">
<description>Pogo Pin Hole - 0.58" 
Specifications:
Pin count:1
Pin pitch:0.58"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_POGOPIN_HOLE_0.58_DIA"/>
</packageinstances>
</package3d>
<package3d name="SNAP-FEMALE" urn="urn:adsk.eagle:package:38032/1" type="box" library_version="1">
<description>Sew-On Fabric Snap - Female
Equivalent to size #1/0 snap. 
Specifications:
Pin count: 1
Area:8mm

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="SNAP-FEMALE"/>
</packageinstances>
</package3d>
<package3d name="SNAP-MALE" urn="urn:adsk.eagle:package:38033/1" type="box" library_version="1">
<description>Sew-On Fabric Snap - Male
Equivalent to size #1/0 snap. 
Specifications:
Pin count: 1
Area:8mm

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="SNAP-MALE"/>
</packageinstances>
</package3d>
<package3d name="SPRING-CONNECTOR" urn="urn:adsk.eagle:package:38034/1" type="box" library_version="1">
<description>Spring Connector
Specifications:
Pin count: 1
Area:0.25"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="SPRING-CONNECTOR"/>
</packageinstances>
</package3d>
<package3d name="1X01NS_KIT" urn="urn:adsk.eagle:package:38038/1" type="box" library_version="1">
<description>Plated Through Hole - No Silk Outline Kit Version
 Mask on only one side to make soldering in kits easier.
Specifications:
Pin count:1
Pin pitch:0.1"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01NS_KIT"/>
</packageinstances>
</package3d>
<package3d name="1X01_NO_SILK" urn="urn:adsk.eagle:package:38041/1" type="box" library_version="1">
<description>Plated Through Hole - No Silk Outline
Specifications:
Pin count:1
Pin pitch:0.1"

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="1X01_NO_SILK"/>
</packageinstances>
</package3d>
<package3d name="SMTSO-256-ET-0.165DIA" urn="urn:adsk.eagle:package:38037/1" type="box" library_version="1">
<description>SMTSO-256-ET Flush Mount Nut
.165 drill

Fits 4-40 Screws. 
Specifications:
Pin count: 1

Example device(s):
CONN_01
</description>
<packageinstances>
<packageinstance name="SMTSO-256-ET-0.165DIA"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ARDUINO_SERIAL_PROGRAM" urn="urn:adsk.eagle:symbol:37587/1" library_version="1">
<description>&lt;h3&gt;6-pin header connection for use with the "FTDI BASIC" pinout - TARGET SIDE.&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

&lt;h3&gt;Also known as "Arduino Serial Program Header".&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

It is used to mate with our FTDI Basic serial programmers using the Arduino IDE. 

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Note, this device has "TARGET" in the name.&lt;/b&gt; It is intended to be used on a design that contains a target micro-controller that you wish to program. 

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;The "sister" device, named "CABLE"&lt;/b&gt; is inteneded to be used on a design that usually lives on the end of your USB cable and prvides the converter chip (FTDI232 or FTDI231x) to commnicate serial RX/TX.</description>
<wire x1="1.27" y1="-7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-7.62" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-7.62" y="10.668" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="DTR" x="5.08" y="-5.08" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="RXI" x="5.08" y="-2.54" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="TXO" x="5.08" y="0" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="VCC" x="5.08" y="2.54" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="CTS" x="5.08" y="5.08" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="GND" x="5.08" y="7.62" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_02" urn="urn:adsk.eagle:symbol:37653/1" library_version="1">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_01" urn="urn:adsk.eagle:symbol:37640/1" library_version="1">
<description>&lt;h3&gt;1 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="6_PIN_SERIAL_TARGET" urn="urn:adsk.eagle:component:38307/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;6-pin header connection for use with the "FTDI BASIC" pinout - TARGET SIDE.&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

&lt;h3&gt;Also known as "Arduino Serial Program Header".&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

It is used to mate with our FTDI Basic serial programmers using the Arduino IDE. 

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Note, this device has "TARGET" in the name.&lt;/b&gt; It is intended to be used on a design that contains a target micro-controller that you wish to program. 

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;The "sister" device, named "CABLE"&lt;/b&gt; is inteneded to be used on a design that usually lives on the end of your USB cable and prvides the converter chip (FTDI232 or FTDI231x) to commnicate serial RX/TX.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with some of these:&lt;/b&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/p&gt;
&lt;p&gt;&lt;/p&gt;
For reference (or to suit your programming needs) check out these designs:
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9716"&gt; SparkFun FTDI Basic Breakout - 5V&lt;/a&gt; (DEV-09716)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9873"&gt; SparkFun FTDI Basic Breakout - 3.3V&lt;/a&gt; (DEV-09873)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12935"&gt; SparkFun FTDI SmartBasic&lt;/a&gt; (DEV-12935)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13746"&gt; SparkFun Beefy 3 - FTDI Basic Breakout&lt;/a&gt; (DEV-13746)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="ARDUINO_SERIAL_PROGRAM" x="0" y="-2.54"/>
</gates>
<devices>
<device name="SMD" package="1X06-SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38004/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08971" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="1X06">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38009/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX_1X6_RA_LOCK">
<connects>
<connect gate="G$1" pin="CTS" pad="2"/>
<connect gate="G$1" pin="DTR" pad="6"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="RXI" pad="5"/>
<connect gate="G$1" pin="TXO" pad="4"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37992/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37991/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38002/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-KIT" package="1X06-KIT">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38006/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SILK" package="6_PIN_SERIAL_TARGET_SIDE_W_SILK">
<connects>
<connect gate="G$1" pin="CTS" pad="P$5"/>
<connect gate="G$1" pin="DTR" pad="P$1"/>
<connect gate="G$1" pin="GND" pad="P$6"/>
<connect gate="G$1" pin="RXI" pad="P$3"/>
<connect gate="G$1" pin="TXO" pad="P$2"/>
<connect gate="G$1" pin="VCC" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37995/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_SMT" package="6_PIN_SERIAL_TARGET_SIDE_RA_SMT">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="DTR" pad="1"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="RXI" pad="3"/>
<connect gate="G$1" pin="TXO" pad="2"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38001/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08971" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02" urn="urn:adsk.eagle:component:38323/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38039/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38040/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38050/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38042/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38043/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38052/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38044/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38045/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38046/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38047/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38049/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38048/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38051/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38053/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38054/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38058/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38055/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38056/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38057/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38061/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38059/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38060/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38062/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_01" urn="urn:adsk.eagle:component:38322/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;Single connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;

&lt;p&gt;&lt;/p&gt;
On any of the 0.1 inch spaced packages, you can populate with these:
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;/p&gt;
This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
SMTSO-256-ET is a "flush mount" nut for a 4-40 screw. We mostly use this on specialty testbeds; it is a nice way to connect hardware to your PCB at an adjustable hieght.
&lt;p&gt;&lt;/p&gt;
Also note, the SNAP packages are for using a snappable style connector. We sell a baggie of snaps and they are also used on two LilyPad designs:
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11347"&gt; Snap Assortment - 30 pack (male and female)&lt;/a&gt; (DEV-11347)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10941"&gt;LilyPad Arduino SimpleSnap&lt;/a&gt; (DEV-10941)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10940"&gt; LilyPad SimpleSnap Protoboard&lt;/a&gt; (DEV-10940)&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_01" x="0" y="0"/>
</gates>
<devices>
<device name="PTH_LONGPAD" package="1X01_LONGPAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38030/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38028/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_2MM" package="1X01_2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38029/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OFFSET" package="1X01_OFFSET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38035/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_LARGE" package="1X01_POGOPIN_HOLE_0.061_DIA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38036/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLE_0.58" package="1X01_POGOPIN_HOLE_0.58_DIA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38031/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-FEMALE" package="SNAP-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38032/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SNAP-MALE" package="SNAP-MALE">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38033/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-CONN" package="SPRING-CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38034/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-11822" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH_NO_SILK_KIT" package="1X01NS_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38038/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_NO_SILK_YES_STOP" package="1X01_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38041/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMTSO-256-ET-0.165DIA" package="SMTSO-256-ET-0.165DIA">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38037/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="HW-08694" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Moteino_export">
<description>Generated from &lt;b&gt;Moteino.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="LOWPOWERLAB_RFM95">
<wire x1="-7.599" y1="-8" x2="7.651" y2="-8" width="0.127" layer="51"/>
<wire x1="7.651" y1="8" x2="-7.599" y2="8" width="0.127" layer="51"/>
<wire x1="3.437" y1="6.735" x2="5.723" y2="6.735" width="0.127" layer="51"/>
<wire x1="5.723" y1="6.735" x2="5.723" y2="3.0012" width="0.127" layer="51"/>
<wire x1="5.723" y1="3.0012" x2="3.437" y2="3.0012" width="0.127" layer="51"/>
<wire x1="3.437" y1="3.0012" x2="3.437" y2="6.735" width="0.127" layer="51"/>
<wire x1="-5.2564" y1="6.8754" x2="0.5602" y2="6.8754" width="0.127" layer="51"/>
<wire x1="0.5602" y1="6.8754" x2="0.5602" y2="1.4906" width="0.127" layer="51"/>
<wire x1="0.5602" y1="1.4906" x2="-5.2564" y2="1.4906" width="0.127" layer="51"/>
<wire x1="-5.2564" y1="1.4906" x2="-5.2564" y2="6.8754" width="0.127" layer="51"/>
<smd name="3.3V" x="7.651" y="1" dx="2" dy="1.27" layer="1"/>
<smd name="ANT" x="7.651" y="-7" dx="2" dy="1.27" layer="1"/>
<smd name="DIO0" x="7.651" y="3" dx="2" dy="1.27" layer="1"/>
<smd name="GND" x="7.651" y="-5" dx="2" dy="1.27" layer="1"/>
<smd name="GND1" x="-7.599" y="-7" dx="2" dy="1.27" layer="1"/>
<smd name="GND2" x="-7.599" y="7.032" dx="2" dy="1.27" layer="1"/>
<smd name="MISO" x="-7.599" y="5" dx="2" dy="1.27" layer="1"/>
<smd name="MOSI" x="-7.599" y="2.968" dx="2" dy="1.27" layer="1"/>
<smd name="NSS" x="-7.599" y="-1.096" dx="2" dy="1.27" layer="1"/>
<smd name="SCK" x="-7.599" y="0.936" dx="2" dy="1.27" layer="1"/>
<text x="-2.849" y="10.08" size="1.27" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.929" y="8.65" size="1.27" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<text x="4.2" y="6.4702" size="0.8" layer="51" ratio="15" rot="R270">XTAL</text>
<text x="5.275" y="0.203" size="1.5" layer="51" ratio="15">+</text>
<text x="3.7" y="-7.503" size="1" layer="51" ratio="15">ANT</text>
<text x="-3.7216" y="3.6506" size="0.8" layer="51" ratio="15">RF95</text>
</package>
<package name="LOWPOWERLAB_RFM95_LONGER">
<wire x1="-8.107" y1="-8" x2="7.651" y2="-8" width="0.127" layer="51"/>
<wire x1="7.651" y1="8" x2="-8.107" y2="8" width="0.127" layer="51"/>
<wire x1="3.437" y1="6.735" x2="5.723" y2="6.735" width="0.127" layer="51"/>
<wire x1="5.723" y1="6.735" x2="5.723" y2="3.0012" width="0.127" layer="51"/>
<wire x1="5.723" y1="3.0012" x2="3.437" y2="3.0012" width="0.127" layer="51"/>
<wire x1="3.437" y1="3.0012" x2="3.437" y2="6.735" width="0.127" layer="51"/>
<wire x1="-5.2564" y1="6.8754" x2="0.5602" y2="6.8754" width="0.127" layer="51"/>
<wire x1="0.5602" y1="6.8754" x2="0.5602" y2="1.4906" width="0.127" layer="51"/>
<wire x1="0.5602" y1="1.4906" x2="-5.2564" y2="1.4906" width="0.127" layer="51"/>
<wire x1="-5.2564" y1="1.4906" x2="-5.2564" y2="6.8754" width="0.127" layer="51"/>
<smd name="3.3V" x="7.651" y="1" dx="2" dy="1.27" layer="1"/>
<smd name="ANT" x="7.651" y="-7" dx="2" dy="1.27" layer="1"/>
<smd name="DIO0" x="7.651" y="3" dx="2" dy="1.27" layer="1"/>
<smd name="GND" x="7.651" y="-5" dx="2" dy="1.27" layer="1"/>
<smd name="GND1" x="-8.107" y="-7" dx="2" dy="1.27" layer="1"/>
<smd name="GND2" x="-8.107" y="7.032" dx="2" dy="1.27" layer="1"/>
<smd name="MISO" x="-8.107" y="5" dx="2" dy="1.27" layer="1"/>
<smd name="MOSI" x="-8.107" y="2.968" dx="2" dy="1.27" layer="1"/>
<smd name="NSS" x="-8.107" y="-1.096" dx="2" dy="1.27" layer="1"/>
<smd name="SCK" x="-8.107" y="0.936" dx="2" dy="1.27" layer="1"/>
<text x="-2.849" y="10.08" size="1.27" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.929" y="8.65" size="1.27" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<text x="4.2" y="6.4702" size="0.8" layer="51" ratio="15" rot="R270">XTAL</text>
<text x="5.275" y="0.203" size="1.5" layer="51" ratio="15">+</text>
<text x="3.7" y="-7.503" size="1" layer="51" ratio="15">ANT</text>
<text x="-3.7216" y="3.6506" size="0.8" layer="51" ratio="15">RF95</text>
</package>
</packages>
<symbols>
<symbol name="LOWPOWERLAB_RFM95">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="ANT" x="12.7" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="DIO0" x="-12.7" y="-5.08" visible="pad" length="short" direction="oc"/>
<pin name="GND" x="0" y="-12.7" length="short" direction="pwr" rot="R90"/>
<pin name="GND1" x="2.54" y="-12.7" length="short" direction="pwr" rot="R90"/>
<pin name="GND2" x="5.08" y="-12.7" length="short" direction="pwr" rot="R90"/>
<pin name="MISO" x="-12.7" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="MOSI" x="-12.7" y="0" visible="pad" length="short" direction="out"/>
<pin name="SCK" x="-12.7" y="5.08" visible="pad" length="short" direction="in"/>
<pin name="SEL" x="-12.7" y="7.62" visible="pad" length="short" direction="in"/>
<pin name="VCC" x="0" y="15.24" length="short" direction="pwr" rot="R270"/>
<text x="-5.08" y="0" size="1.778" layer="94">RFM95</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOWPOWERLAB_RFM95">
<gates>
<gate name="G$1" symbol="LOWPOWERLAB_RFM95" x="0" y="-2.54"/>
</gates>
<devices>
<device name="COMPACT" package="LOWPOWERLAB_RFM95">
<connects>
<connect gate="G$1" pin="ANT" pad="ANT"/>
<connect gate="G$1" pin="DIO0" pad="DIO0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="SEL" pad="NSS"/>
<connect gate="G$1" pin="VCC" pad="3.3V"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGER" package="LOWPOWERLAB_RFM95_LONGER">
<connects>
<connect gate="G$1" pin="ANT" pad="ANT"/>
<connect gate="G$1" pin="DIO0" pad="DIO0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="SEL" pad="NSS"/>
<connect gate="G$1" pin="VCC" pad="3.3V"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CB">
<description>Upverter Parts Library

Created by Upverter.com</description>
<packages>
<package name="MICROCHIP_MCP1703T-3002E-CB_0">
<description>PM-LINEREG-MCP1703-IL3A</description>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="-0.7" width="0.15" layer="21"/>
<wire x1="-0.1" y1="-1.55" x2="0.9" y2="-1.55" width="0.15" layer="21"/>
<wire x1="0.9" y1="0.7" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="-0.1" y1="1.55" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="-0.9" y1="-1.55" x2="0.9" y2="-1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="-1.55" x2="-0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-1.8" y1="-1.65" x2="-1.8" y2="1.65" width="0.1" layer="39"/>
<wire x1="-1.8" y1="1.65" x2="1.8" y2="1.65" width="0.1" layer="39"/>
<wire x1="1.8" y1="1.65" x2="1.8" y2="-1.65" width="0.1" layer="39"/>
<wire x1="1.8" y1="-1.65" x2="-1.8" y2="-1.65" width="0.1" layer="39"/>
<text x="-2.8" y="1.925" size="1" layer="25">&gt;NAME</text>
<circle x="-1.1" y="1.8" radius="0.125" width="0.25" layer="21"/>
<smd name="3" x="1.1" y="0" dx="0.6" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="-1.1" y="-0.95" dx="0.6" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="-1.1" y="0.95" dx="0.6" dy="1.2" layer="1" rot="R90"/>
</package>
<package name="MICROCHIP_MCP1703T-3002E-CB_1">
<description>PM-LINEREG-MCP1703-IL3A</description>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="-0.7" width="0.15" layer="21"/>
<wire x1="0" y1="-1.55" x2="0.9" y2="-1.55" width="0.15" layer="21"/>
<wire x1="0.9" y1="0.7" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="0" y1="1.55" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="-0.9" y1="-1.55" x2="0.9" y2="-1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="-1.55" x2="-0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-2.15" y1="-1.8" x2="-2.15" y2="1.8" width="0.1" layer="39"/>
<wire x1="-2.15" y1="1.8" x2="2.15" y2="1.8" width="0.1" layer="39"/>
<wire x1="2.15" y1="1.8" x2="2.15" y2="-1.8" width="0.1" layer="39"/>
<wire x1="2.15" y1="-1.8" x2="-2.15" y2="-1.8" width="0.1" layer="39"/>
<text x="-3.15" y="1.925" size="1" layer="25">&gt;NAME</text>
<circle x="-1.15" y="1.8" radius="0.125" width="0.25" layer="21"/>
<smd name="3" x="1.15" y="0" dx="0.6" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="-1.15" y="-0.95" dx="0.6" dy="1.5" layer="1" rot="R90"/>
<smd name="1" x="-1.15" y="0.95" dx="0.6" dy="1.5" layer="1" rot="R90"/>
</package>
<package name="MICROCHIP_MCP1703T-3002E-CB_2">
<description>PM-LINEREG-MCP1703-IL3A</description>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="-0.7" width="0.15" layer="21"/>
<wire x1="0.1" y1="-1.55" x2="0.9" y2="-1.55" width="0.15" layer="21"/>
<wire x1="0.9" y1="0.7" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="0.1" y1="1.55" x2="0.9" y2="1.55" width="0.15" layer="21"/>
<wire x1="-0.9" y1="-1.55" x2="0.9" y2="-1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="0.9" y1="-1.55" x2="0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-0.9" y1="-1.55" x2="-0.9" y2="1.55" width="0.1" layer="51"/>
<wire x1="-2.6" y1="-2.05" x2="-2.6" y2="2.05" width="0.1" layer="39"/>
<wire x1="-2.6" y1="2.05" x2="2.6" y2="2.05" width="0.1" layer="39"/>
<wire x1="2.6" y1="2.05" x2="2.6" y2="-2.05" width="0.1" layer="39"/>
<wire x1="2.6" y1="-2.05" x2="-2.6" y2="-2.05" width="0.1" layer="39"/>
<text x="-3.6" y="2.05" size="1" layer="25">&gt;NAME</text>
<circle x="-1.2" y="1.8" radius="0.125" width="0.25" layer="21"/>
<smd name="3" x="1.2" y="0" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="-1.2" y="-0.95" dx="0.6" dy="1.8" layer="1" rot="R90"/>
<smd name="1" x="-1.2" y="0.95" dx="0.6" dy="1.8" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="MICROCHIP_MCP1703T-3002E-CB_0_0">
<description>PM-LINEREG-MCP1703-IL3A</description>
<wire x1="0" y1="-15.24" x2="0" y2="-5.08" width="0.508" layer="94"/>
<wire x1="0" y1="-5.08" x2="15.24" y2="-5.08" width="0.508" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="-15.24" width="0.508" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="0" y2="-15.24" width="0.508" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.15" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="-7.62" width="0.15" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-7.62" width="0.15" layer="94"/>
<text x="0" y="-2.54" size="2.54" layer="95" align="top-left">&gt;NAME</text>
<text x="0" y="-20.32" size="2.54" layer="95" align="top-left">MCP1703T-3002E/CB</text>
<pin name="GND" x="20.32" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="VOUT" x="20.32" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN" x="-5.08" y="-7.62" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MICROCHIP_MCP1703T-3002E-CB" prefix="U">
<description>PM-LINEREG-MCP1703-IL3A</description>
<gates>
<gate name="G$0" symbol="MICROCHIP_MCP1703T-3002E-CB_0_0" x="0" y="0"/>
</gates>
<devices>
<device name="MICROCHIP_MCP1703T-3002E-CB_0_0" package="MICROCHIP_MCP1703T-3002E-CB_0">
<connects>
<connect gate="G$0" pin="GND" pad="1"/>
<connect gate="G$0" pin="VIN" pad="3"/>
<connect gate="G$0" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CIIVA_IDS" value="1500876"/>
<attribute name="DATASHEET_VERSION" value="revD, Sep-2009"/>
<attribute name="FOOTPRINT_VARIANT_NAME_0" value="Level C"/>
<attribute name="FOOTPRINT_VARIANT_NAME_1" value="Level B"/>
<attribute name="FOOTPRINT_VARIANT_NAME_2" value="Level A"/>
<attribute name="IMPORTED" value="yes"/>
<attribute name="IMPORTED_FROM" value="vault"/>
<attribute name="IMPORT_TS" value="1521844496"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MPN" value="MCP1703T-3002E/CB"/>
<attribute name="PACKAGE" value="SOT-23A-CB3"/>
<attribute name="PACKAGE_DESCRIPTION" value="3-Lead Plastic Small Outline Transistor (CB) [SOT-23A]"/>
<attribute name="PACKAGE_VERSION" value="revBB, Aug-2009"/>
<attribute name="PREFIX" value="U"/>
<attribute name="RELEASE_DATE" value="1300337870"/>
<attribute name="VAULT_GUID" value="FEBC9CC7-48A5-4106-B997-93AC78A104E9"/>
<attribute name="VAULT_REVISION" value="F3127F12-6D39-427F-BC50-83B884144855"/>
</technology>
</technologies>
</device>
<device name="MICROCHIP_MCP1703T-3002E-CB_0_1" package="MICROCHIP_MCP1703T-3002E-CB_1">
<connects>
<connect gate="G$0" pin="GND" pad="1"/>
<connect gate="G$0" pin="VIN" pad="3"/>
<connect gate="G$0" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CIIVA_IDS" value="1500876"/>
<attribute name="DATASHEET_VERSION" value="revD, Sep-2009"/>
<attribute name="FOOTPRINT_VARIANT_NAME_0" value="Level C"/>
<attribute name="FOOTPRINT_VARIANT_NAME_1" value="Level B"/>
<attribute name="FOOTPRINT_VARIANT_NAME_2" value="Level A"/>
<attribute name="IMPORTED" value="yes"/>
<attribute name="IMPORTED_FROM" value="vault"/>
<attribute name="IMPORT_TS" value="1521844496"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MPN" value="MCP1703T-3002E/CB"/>
<attribute name="PACKAGE" value="SOT-23A-CB3"/>
<attribute name="PACKAGE_DESCRIPTION" value="3-Lead Plastic Small Outline Transistor (CB) [SOT-23A]"/>
<attribute name="PACKAGE_VERSION" value="revBB, Aug-2009"/>
<attribute name="PREFIX" value="U"/>
<attribute name="RELEASE_DATE" value="1300337870"/>
<attribute name="VAULT_GUID" value="FEBC9CC7-48A5-4106-B997-93AC78A104E9"/>
<attribute name="VAULT_REVISION" value="F3127F12-6D39-427F-BC50-83B884144855"/>
</technology>
</technologies>
</device>
<device name="MICROCHIP_MCP1703T-3002E-CB_0_2" package="MICROCHIP_MCP1703T-3002E-CB_2">
<connects>
<connect gate="G$0" pin="GND" pad="1"/>
<connect gate="G$0" pin="VIN" pad="3"/>
<connect gate="G$0" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CIIVA_IDS" value="1500876"/>
<attribute name="DATASHEET_VERSION" value="revD, Sep-2009"/>
<attribute name="FOOTPRINT_VARIANT_NAME_0" value="Level C"/>
<attribute name="FOOTPRINT_VARIANT_NAME_1" value="Level B"/>
<attribute name="FOOTPRINT_VARIANT_NAME_2" value="Level A"/>
<attribute name="IMPORTED" value="yes"/>
<attribute name="IMPORTED_FROM" value="vault"/>
<attribute name="IMPORT_TS" value="1521844496"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MPN" value="MCP1703T-3002E/CB"/>
<attribute name="PACKAGE" value="SOT-23A-CB3"/>
<attribute name="PACKAGE_DESCRIPTION" value="3-Lead Plastic Small Outline Transistor (CB) [SOT-23A]"/>
<attribute name="PACKAGE_VERSION" value="revBB, Aug-2009"/>
<attribute name="PREFIX" value="U"/>
<attribute name="RELEASE_DATE" value="1300337870"/>
<attribute name="VAULT_GUID" value="FEBC9CC7-48A5-4106-B997-93AC78A104E9"/>
<attribute name="VAULT_REVISION" value="F3127F12-6D39-427F-BC50-83B884144855"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="MF_IC_Digital" deviceset="ATMEGA328P" device="-AU" value="ATMEGA328PB-AU">
<attribute name="MPN" value="ATMEGA328PB-AU "/>
</part>
<part name="R1" library="MF_Passives" deviceset="RESISTOR" device="_0603" value="10K">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-RES-0603-10K"/>
</part>
<part name="U$1" library="MF_Aesthetics" deviceset="POWER_RAIL" device="" value="VCC"/>
<part name="X1" library="MF_Frequency_Control" deviceset="CYRSTAL_GND" device="_ABMM">
<attribute name="MPN" value="ABMM-8.000MHZ-B2-T "/>
</part>
<part name="U$2" library="MF_Aesthetics" deviceset="GND_RAIL" device="" value="GND"/>
<part name="C2" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="18pF">
<attribute name="HOUSEPART" value="1"/>
<attribute name="MPN" value="MF-CAP-0603-18pF"/>
</part>
<part name="C3" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="0.1uF">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C5" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="0.1uF">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C6" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="0.1uF">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C7" library="MF_Passives" deviceset="CAPACITOR_NP" device="_1206" value="10uF">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-1206-10uF"/>
</part>
<part name="RESET" library="MF_Switches" deviceset="TACT" device="_4.2MM" value="MF-SW-TACT-4.2MM"/>
<part name="D1" library="MF_LEDs" deviceset="LED_SINGLE" device="-0805-RED" value="MF-LED-0805-RED"/>
<part name="R2" library="MF_Passives" deviceset="RESISTOR" device="_0603" value="360">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-RES-0603-360"/>
</part>
<part name="IOH" library="MF_Connectors" deviceset="CON_01X08" device="_PTH_2.54MM" value="MF-CON-2.54mm-01x08">
<attribute name="LABEL01" value="AREF"/>
<attribute name="LABEL02" value="GND"/>
<attribute name="LABEL03" value="13"/>
<attribute name="LABEL04" value="12"/>
<attribute name="LABEL05" value="~11"/>
<attribute name="LABEL06" value="~10"/>
<attribute name="LABEL07" value="~9"/>
<attribute name="LABEL08" value="8"/>
</part>
<part name="IOL" library="MF_Connectors" deviceset="CON_01X08" device="_PTH_2.54MM" value="MF-CON-2.54mm-01x08">
<attribute name="LABEL01" value="7"/>
<attribute name="LABEL02" value="~6"/>
<attribute name="LABEL03" value="~5"/>
<attribute name="LABEL04" value="4"/>
<attribute name="LABEL05" value="~3"/>
<attribute name="LABEL06" value="2"/>
<attribute name="LABEL07" value="TX-&gt;1"/>
<attribute name="LABEL08" value="RX&lt;-0"/>
</part>
<part name="AD" library="MF_Connectors" deviceset="CON_01X06" device="_PTH_2.54MM" value="MF-CON-2.54mm-01x06">
<attribute name="LABEL01" value="A5"/>
<attribute name="LABEL02" value="A4"/>
<attribute name="LABEL03" value="A3"/>
<attribute name="LABEL04" value="A2"/>
<attribute name="LABEL05" value="A1"/>
<attribute name="LABEL06" value="A0"/>
</part>
<part name="POWER" library="MF_Connectors" deviceset="CON_01X06" device="_PTH_2.54MM" value="MF-CON-2.54mm-01x06">
<attribute name="LABEL01" value="RESET"/>
<attribute name="LABEL02" value="3.3V"/>
<attribute name="LABEL03" value="5V"/>
<attribute name="LABEL04" value="GND"/>
<attribute name="LABEL05" value="GND"/>
<attribute name="LABEL06" value="Vin"/>
</part>
<part name="C8" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="0.1uF">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="ISP" library="MF_Connectors" deviceset="CON_02X03" device="_PTH_2.54MM" value="MF-CON-2.54mm-2x3">
<attribute name="LABEL01" value="MISO"/>
<attribute name="LABEL02" value="VCC"/>
<attribute name="LABEL03" value="SCK"/>
<attribute name="LABEL04" value="MOSI"/>
<attribute name="LABEL05" value="RST"/>
<attribute name="LABEL06" value="GND"/>
</part>
<part name="U2" library="Texas Instruments - DRV8835DSSR" deviceset="TEXAS_INSTRUMENTS_DRV8835DSSR" device="TEXAS_INSTRUMENTS_DRV8835DSSR_0_0"/>
<part name="J1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="6_PIN_SERIAL_TARGET" device="LOCK" package3d_urn="urn:adsk.eagle:package:38002/1"/>
<part name="J2" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CONN_02" device="5MM" package3d_urn="urn:adsk.eagle:package:38044/1"/>
<part name="J3" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CONN_02" device="5MM" package3d_urn="urn:adsk.eagle:package:38044/1"/>
<part name="U$3" library="Moteino_export" deviceset="LOWPOWERLAB_RFM95" device="LONGER"/>
<part name="J4" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CONN_01" device="" package3d_urn="urn:adsk.eagle:package:38028/1"/>
<part name="U3" library="CB" deviceset="MICROCHIP_MCP1703T-3002E-CB" device="MICROCHIP_MCP1703T-3002E-CB_0_2"/>
<part name="J5" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CONN_02" device="5MM" package3d_urn="urn:adsk.eagle:package:38044/1"/>
<part name="C12" library="MF_Passives" deviceset="CAPACITOR_NP" device="_1206">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-1206-10uF"/>
</part>
<part name="C11" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C13" library="MF_Passives" deviceset="CAPACITOR_NP" device="_1206">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-1206-10uF"/>
</part>
<part name="C14" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C15" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-0.1uF"/>
</part>
<part name="C9" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-0603-1uF"/>
</part>
<part name="C4" library="MF_Passives" deviceset="CAPACITOR_P" device="_12.5MM">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-12.5MM-1000uF"/>
</part>
<part name="C10" library="MF_Passives" deviceset="CAPACITOR_P" device="_12.5MM">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-12.5MM-1000uF"/>
</part>
<part name="C16" library="MF_Passives" deviceset="CAPACITOR_P" device="_12.5MM">
<attribute name="HOUSEPART" value="YES"/>
<attribute name="MPN" value="MF-CAP-12.5MM-1000uF"/>
</part>
<part name="D2" library="MF_LEDs" deviceset="LED_SINGLE" device="-0805-RED" value="MF-LED-0805-RED"/>
<part name="R3" library="MF_Passives" deviceset="RESISTOR" device="_0402">
<attribute name="HOUSEPART" value="1"/>
<attribute name="MPN" value="MF-RES-0402-560"/>
</part>
<part name="D3" library="MF_LEDs" deviceset="LED_SINGLE" device="-0805-RED">
<attribute name="MPN" value="MF-LED-0805-GREEN"/>
</part>
<part name="R4" library="MF_Passives" deviceset="RESISTOR" device="_0402">
<attribute name="HOUSEPART" value="1"/>
<attribute name="MPN" value="MF-RES-0402-560"/>
</part>
<part name="D4" library="MF_LEDs" deviceset="LED_SINGLE" device="-0805-RED" value="MF-LED-0805-RED"/>
<part name="D5" library="MF_LEDs" deviceset="LED_SINGLE" device="-0805-RED">
<attribute name="MPN" value="MF-LED-0805-GREEN"/>
</part>
<part name="C1" library="MF_Passives" deviceset="CAPACITOR_NP" device="_0603" value="18pF">
<attribute name="HOUSEPART" value="1"/>
<attribute name="MPN" value="MF-CAP-0603-18pF"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="30.48" y="78.74" smashed="yes">
<attribute name="NAME" x="30.48" y="83.82" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="30.48" y="81.28" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R1" gate="G$1" x="106.68" y="45.72" smashed="yes">
<attribute name="NAME" x="109.22" y="47.244" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="U$1" gate="G$1" x="-30.48" y="96.52" smashed="yes">
<attribute name="VALUE" x="-30.48" y="99.06" size="1.016" layer="96" font="vector" align="center"/>
</instance>
<instance part="X1" gate="G$1" x="86.36" y="55.88" smashed="yes">
<attribute name="NAME" x="83.82" y="55.88" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="81.28" y="58.42" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="83.82" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="-30.48" y="-7.62" smashed="yes">
<attribute name="VALUE" x="-30.48" y="-10.16" size="1.016" layer="96" font="vector" rot="R180" align="center"/>
</instance>
<instance part="C2" gate="G$1" x="78.74" y="48.26" smashed="yes">
<attribute name="NAME" x="81.28" y="49.784" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="MPN" x="76.2" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="-30.48" y="81.28" smashed="yes">
<attribute name="NAME" x="-27.94" y="82.804" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="C5" gate="G$1" x="-10.16" y="81.28" smashed="yes">
<attribute name="NAME" x="-7.62" y="82.804" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="C6" gate="G$1" x="0" y="81.28" smashed="yes">
<attribute name="NAME" x="2.54" y="82.804" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="C7" gate="G$1" x="10.16" y="81.28" smashed="yes">
<attribute name="NAME" x="12.7" y="82.804" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="RESET" gate="G$1" x="121.92" y="35.56" smashed="yes">
<attribute name="NAME" x="119.38" y="43.18" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="119.38" y="40.64" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="D1" gate="G$1" x="101.6" y="12.7" smashed="yes" rot="R270">
<attribute name="NAME" x="99.06" y="15.24" size="1.016" layer="95" font="vector" rot="R270" align="top-left"/>
<attribute name="VALUE" x="96.52" y="15.24" size="1.016" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="R2" gate="G$1" x="101.6" y="27.94" smashed="yes">
<attribute name="NAME" x="104.14" y="29.464" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="IOH" gate="G$1" x="187.96" y="66.04" smashed="yes">
<attribute name="NAME" x="187.96" y="71.12" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="187.96" y="68.58" size="1.016" layer="96" font="vector"/>
<attribute name="LABEL05" x="195.58" y="55.88" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL04" x="195.58" y="58.42" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL03" x="195.58" y="60.96" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL01" x="195.58" y="66.04" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL08" x="195.58" y="48.26" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL02" x="195.58" y="63.5" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL06" x="195.58" y="53.34" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL07" x="195.58" y="50.8" size="1.016" layer="97" font="vector" align="center-left"/>
</instance>
<instance part="IOL" gate="G$1" x="187.96" y="20.32" smashed="yes">
<attribute name="NAME" x="187.96" y="25.4" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="187.96" y="22.86" size="1.016" layer="96" font="vector"/>
<attribute name="LABEL05" x="195.58" y="10.16" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL04" x="195.58" y="12.7" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL03" x="195.58" y="15.24" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL01" x="195.58" y="20.32" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL08" x="195.58" y="2.54" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL02" x="195.58" y="17.78" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL06" x="195.58" y="7.62" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL07" x="195.58" y="5.08" size="1.016" layer="97" font="vector" align="center-left"/>
</instance>
<instance part="AD" gate="G$1" x="187.96" y="40.64" smashed="yes">
<attribute name="NAME" x="187.96" y="45.72" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="187.96" y="43.18" size="1.016" layer="96" font="vector"/>
<attribute name="LABEL05" x="195.58" y="30.48" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL04" x="195.58" y="33.02" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL03" x="195.58" y="35.56" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL01" x="195.58" y="40.64" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL06" x="195.58" y="27.94" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL02" x="195.58" y="38.1" size="1.016" layer="97" font="vector" align="center-left"/>
</instance>
<instance part="POWER" gate="G$1" x="187.96" y="86.36" smashed="yes">
<attribute name="NAME" x="187.96" y="91.44" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="187.96" y="88.9" size="1.016" layer="96" font="vector"/>
<attribute name="LABEL05" x="195.58" y="76.2" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL04" x="195.58" y="78.74" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL03" x="195.58" y="81.28" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL01" x="195.58" y="86.36" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL06" x="195.58" y="73.66" size="1.016" layer="97" font="vector" align="center-left"/>
<attribute name="LABEL02" x="195.58" y="83.82" size="1.016" layer="97" font="vector" align="center-left"/>
</instance>
<instance part="C8" gate="G$1" x="12.7" y="48.26" smashed="yes">
<attribute name="NAME" x="15.24" y="49.784" size="1.016" layer="95" font="vector" align="top-left"/>
</instance>
<instance part="ISP" gate="G$1" x="175.26" y="121.92" smashed="yes">
<attribute name="LABEL01" x="182.88" y="121.92" size="1.016" layer="97" font="vector" rot="MR180" align="center-left"/>
<attribute name="NAME" x="175.26" y="127" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="175.26" y="124.46" size="1.016" layer="96" font="vector"/>
<attribute name="LABEL02" x="198.12" y="121.92" size="1.016" layer="97" font="vector" rot="MR0" align="center-left"/>
<attribute name="LABEL03" x="182.88" y="119.38" size="1.016" layer="97" font="vector" rot="MR180" align="center-left"/>
<attribute name="LABEL04" x="198.12" y="119.38" size="1.016" layer="97" font="vector" rot="MR0" align="center-left"/>
<attribute name="LABEL05" x="182.88" y="116.84" size="1.016" layer="97" font="vector" rot="MR180" align="center-left"/>
<attribute name="LABEL06" x="198.12" y="116.84" size="1.016" layer="97" font="vector" rot="MR0" align="center-left"/>
</instance>
<instance part="U2" gate="G$0" x="246.38" y="93.98" smashed="yes">
<attribute name="NAME" x="248.92" y="91.44" size="2.54" layer="95" align="top-left"/>
</instance>
<instance part="J1" gate="G$1" x="190.5" y="-12.7" smashed="yes" rot="R180">
<attribute name="VALUE" x="198.12" y="-2.794" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="198.12" y="-23.368" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="G$1" x="309.88" y="86.36" smashed="yes" rot="R180">
<attribute name="VALUE" x="312.42" y="91.186" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="312.42" y="80.772" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="G$1" x="309.88" y="78.74" smashed="yes" rot="R180">
<attribute name="VALUE" x="312.42" y="83.566" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="312.42" y="73.152" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$3" gate="G$1" x="261.62" y="17.78" smashed="yes"/>
<instance part="J4" gate="G$1" x="284.48" y="20.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="287.02" y="25.146" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="287.02" y="17.272" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U3" gate="G$0" x="-96.52" y="83.82" smashed="yes">
<attribute name="NAME" x="-96.52" y="81.28" size="2.54" layer="95" align="top-left"/>
</instance>
<instance part="J5" gate="G$1" x="236.22" y="111.76" smashed="yes">
<attribute name="VALUE" x="233.68" y="106.934" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="233.68" y="117.348" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C12" gate="G$1" x="-124.46" y="71.12" smashed="yes">
<attribute name="NAME" x="-124.46" y="67.564" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="-121.92" y="69.596" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="-127" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="-109.22" y="71.12" smashed="yes">
<attribute name="NAME" x="-106.68" y="70.104" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="-106.68" y="69.596" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="-109.22" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="-71.12" y="71.12" smashed="yes">
<attribute name="NAME" x="-71.12" y="67.564" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="-68.58" y="69.596" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="-73.66" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="-66.04" y="71.12" smashed="yes">
<attribute name="NAME" x="-63.5" y="70.104" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="-63.5" y="69.596" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="-66.04" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="121.92" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="122.936" y="50.8" size="1.016" layer="95" font="vector" rot="R90" align="top-left"/>
<attribute name="VALUE" x="123.444" y="50.8" size="1.016" layer="96" font="vector" rot="R90"/>
<attribute name="MPN" x="127" y="48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="124.46" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="125.476" y="58.42" size="1.016" layer="95" font="vector" rot="R90" align="top-left"/>
<attribute name="VALUE" x="125.984" y="58.42" size="1.016" layer="96" font="vector" rot="R90"/>
<attribute name="MPN" x="129.54" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="215.9" y="78.74" smashed="yes">
<attribute name="NAME" x="218.44" y="80.264" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="218.44" y="77.216" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C10" gate="G$1" x="220.98" y="78.74" smashed="yes">
<attribute name="NAME" x="223.52" y="80.264" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="223.52" y="77.216" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="C16" gate="G$1" x="210.82" y="78.74" smashed="yes">
<attribute name="NAME" x="213.36" y="80.264" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="213.36" y="77.216" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="D2" gate="G$1" x="322.58" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="325.12" y="73.66" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="327.66" y="73.66" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R3" gate="G$1" x="322.58" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="325.12" y="79.756" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="324.104" y="81.28" size="1.016" layer="96" font="vector" rot="R90"/>
<attribute name="MPN" x="332.74" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="322.58" y="68.58" smashed="yes">
<attribute name="NAME" x="320.04" y="68.58" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="317.5" y="71.12" size="1.016" layer="96" font="vector"/>
<attribute name="MPN" x="320.04" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="322.58" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="325.12" y="84.836" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="324.104" y="86.36" size="1.016" layer="96" font="vector" rot="R90"/>
<attribute name="MPN" x="332.74" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="322.58" y="88.9" smashed="yes">
<attribute name="NAME" x="320.04" y="88.9" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="317.5" y="88.9" size="1.016" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="D5" gate="G$1" x="322.58" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="325.12" y="93.98" size="1.016" layer="95" font="vector" rot="R180" align="top-left"/>
<attribute name="VALUE" x="327.66" y="91.44" size="1.016" layer="96" font="vector" rot="R180"/>
<attribute name="MPN" x="317.5" y="91.44" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="93.98" y="48.26" smashed="yes">
<attribute name="NAME" x="96.52" y="49.784" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="MPN" x="91.44" y="43.18" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="MISO" class="0">
<segment>
<wire x1="119.38" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="76.2" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB4"/>
<wire x1="111.76" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<label x="66.04" y="66.04" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN4"/>
<wire x1="172.72" y1="58.42" x2="185.42" y2="58.42" width="0.1524" layer="91"/>
<label x="175.26" y="58.42" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN1"/>
<wire x1="172.72" y1="121.92" x2="165.1" y2="121.92" width="0.1524" layer="91"/>
<label x="165.1" y="121.92" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="MISO"/>
<wire x1="248.92" y1="20.32" x2="238.76" y2="20.32" width="0.1524" layer="91"/>
<label x="238.76" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5"/>
<wire x1="63.5" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="101.6" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="114.3" y1="63.5" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<label x="66.04" y="63.5" size="1.016" layer="95" font="vector"/>
<pinref part="R2" gate="G$1" pin="P$1"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="101.6" y="63.5"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN3"/>
<wire x1="185.42" y1="60.96" x2="172.72" y2="60.96" width="0.1524" layer="91"/>
<label x="175.26" y="60.96" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN3"/>
<wire x1="165.1" y1="119.38" x2="172.72" y2="119.38" width="0.1524" layer="91"/>
<label x="165.1" y="119.38" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="SCK"/>
<wire x1="248.92" y1="22.86" x2="238.76" y2="22.86" width="0.1524" layer="91"/>
<label x="238.76" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="154.94" y1="73.66" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="73.66" x2="160.02" y2="83.82" width="0.1524" layer="91"/>
<wire x1="160.02" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PB3"/>
<wire x1="109.22" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<label x="66.04" y="68.58" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN5"/>
<wire x1="185.42" y1="55.88" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<label x="175.26" y="55.88" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN4"/>
<wire x1="215.9" y1="119.38" x2="208.28" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="MOSI"/>
<wire x1="248.92" y1="17.78" x2="238.76" y2="17.78" width="0.1524" layer="91"/>
<label x="238.76" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC6"/>
<wire x1="63.5" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="38.1" x2="116.84" y2="38.1" width="0.1524" layer="91"/>
<wire x1="116.84" y1="38.1" x2="116.84" y2="48.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="48.26" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<wire x1="116.84" y1="55.88" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<label x="66.04" y="38.1" size="1.016" layer="95" font="vector"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="106.68" y1="40.64" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<junction x="106.68" y="38.1"/>
<pinref part="RESET" gate="G$1" pin="P$1"/>
<wire x1="116.84" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<junction x="116.84" y="38.1"/>
<wire x1="121.92" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<junction x="116.84" y="55.88"/>
<wire x1="119.38" y1="48.26" x2="116.84" y2="48.26" width="0.1524" layer="91"/>
<junction x="116.84" y="48.26"/>
<pinref part="C15" gate="G$1" pin="P$1"/>
<pinref part="C9" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="POWER" gate="G$1" pin="PIN1"/>
<wire x1="185.42" y1="86.36" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<label x="175.26" y="86.36" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN5"/>
<wire x1="172.72" y1="116.84" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<label x="165.1" y="116.84" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="5.0V" class="0">
<segment>
<pinref part="POWER" gate="G$1" pin="PIN3"/>
<wire x1="185.42" y1="81.28" x2="172.72" y2="81.28" width="0.1524" layer="91"/>
<label x="175.26" y="81.28" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB7"/>
<wire x1="63.5" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="X0"/>
<wire x1="78.74" y1="58.42" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="50.8" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<junction x="78.74" y="55.88"/>
<label x="66.04" y="58.42" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB6"/>
<wire x1="63.5" y1="60.96" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<wire x1="99.06" y1="60.96" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="X1"/>
<wire x1="99.06" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="93.98" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<wire x1="93.98" y1="50.8" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<junction x="93.98" y="55.88"/>
<label x="66.04" y="60.96" size="1.016" layer="95" font="vector"/>
<pinref part="C1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="86.36" y1="50.8" x2="86.36" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-2.54" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<wire x1="12.7" y1="-2.54" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="78.74" y1="45.72" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-2.54" x2="93.98" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-2.54" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<junction x="86.36" y="-2.54"/>
<junction x="78.74" y="-2.54"/>
<pinref part="U$2" gate="G$1" pin="P$1"/>
<wire x1="-30.48" y1="-5.08" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-2.54" x2="101.6" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-2.54" x2="116.84" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-2.54" x2="160.02" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-2.54" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
<wire x1="160.02" y1="43.18" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="160.02" y1="71.12" x2="154.94" y2="71.12" width="0.1524" layer="91"/>
<junction x="93.98" y="-2.54"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="10.16" y1="71.12" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="10.16" y1="78.74" x2="10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="10.16" y1="71.12" x2="0" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="P$2"/>
<wire x1="0" y1="78.74" x2="0" y2="71.12" width="0.1524" layer="91"/>
<wire x1="0" y1="71.12" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="-10.16" y1="78.74" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="71.12" x2="-30.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="71.12" x2="-30.48" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="71.12" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-30.48" y="71.12"/>
<junction x="10.16" y="71.12"/>
<junction x="0" y="71.12"/>
<junction x="-10.16" y="71.12"/>
<junction x="-30.48" y="-2.54"/>
<pinref part="RESET" gate="G$1" pin="P$3"/>
<wire x1="119.38" y1="33.02" x2="116.84" y2="33.02" width="0.1524" layer="91"/>
<wire x1="116.84" y1="33.02" x2="116.84" y2="-2.54" width="0.1524" layer="91"/>
<junction x="116.84" y="-2.54"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<wire x1="12.7" y1="45.72" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<junction x="12.7" y="-2.54"/>
<pinref part="D1" gate="G$1" pin="CATHODE"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="-2.54" width="0.1524" layer="91"/>
<junction x="101.6" y="-2.54"/>
<wire x1="127" y1="48.26" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="43.18" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
<junction x="160.02" y="43.18"/>
<pinref part="C15" gate="G$1" pin="P$2"/>
<wire x1="124.46" y1="48.26" x2="127" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="POWER" gate="G$1" pin="PIN4"/>
<wire x1="172.72" y1="78.74" x2="185.42" y2="78.74" width="0.1524" layer="91"/>
<label x="175.26" y="78.74" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="POWER" gate="G$1" pin="PIN5"/>
<wire x1="185.42" y1="76.2" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<label x="175.26" y="76.2" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN2"/>
<wire x1="172.72" y1="63.5" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<label x="175.26" y="63.5" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN6"/>
<wire x1="208.28" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<label x="210.82" y="116.84" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="GND_PAD"/>
<wire x1="297.18" y1="63.5" x2="314.96" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$0" pin="GND"/>
<wire x1="297.18" y1="66.04" x2="314.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="314.96" y1="63.5" x2="314.96" y2="66.04" width="0.1524" layer="91"/>
<label x="299.72" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="CTS"/>
<wire x1="185.42" y1="-17.78" x2="182.88" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-17.78" x2="182.88" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-20.32" x2="172.72" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="-20.32" x2="185.42" y2="-20.32" width="0.1524" layer="91"/>
<label x="172.72" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="215.9" y1="76.2" x2="215.9" y2="73.66" width="0.1524" layer="91"/>
<label x="215.9" y="68.58" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="P$2"/>
<pinref part="C10" gate="G$1" pin="P$2"/>
<wire x1="215.9" y1="73.66" x2="215.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="220.98" y1="76.2" x2="220.98" y2="73.66" width="0.1524" layer="91"/>
<wire x1="220.98" y1="73.66" x2="215.9" y2="73.66" width="0.1524" layer="91"/>
<junction x="215.9" y="73.66"/>
<wire x1="215.9" y1="73.66" x2="210.82" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="P$2"/>
<wire x1="210.82" y1="73.66" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="261.62" y1="5.08" x2="261.62" y2="2.54" width="0.1524" layer="91"/>
<wire x1="261.62" y1="2.54" x2="264.16" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="GND1"/>
<wire x1="264.16" y1="2.54" x2="266.7" y2="2.54" width="0.1524" layer="91"/>
<wire x1="266.7" y1="2.54" x2="276.86" y2="2.54" width="0.1524" layer="91"/>
<wire x1="264.16" y1="5.08" x2="264.16" y2="2.54" width="0.1524" layer="91"/>
<junction x="264.16" y="2.54"/>
<pinref part="U$3" gate="G$1" pin="GND2"/>
<wire x1="266.7" y1="5.08" x2="266.7" y2="2.54" width="0.1524" layer="91"/>
<junction x="266.7" y="2.54"/>
<label x="269.24" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$0" pin="GND"/>
<label x="-78.74" y="68.58" size="1.778" layer="95"/>
<wire x1="-124.46" y1="68.58" x2="-109.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="68.58" x2="-76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="68.58" x2="-76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="68.58" x2="-71.12" y2="68.58" width="0.1524" layer="91"/>
<junction x="-76.2" y="68.58"/>
<pinref part="C12" gate="G$1" pin="P$2"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<junction x="-109.22" y="68.58"/>
<pinref part="C13" gate="G$1" pin="P$2"/>
<wire x1="-71.12" y1="68.58" x2="-66.04" y2="68.58" width="0.1524" layer="91"/>
<junction x="-71.12" y="68.58"/>
<pinref part="C14" gate="G$1" pin="P$2"/>
<wire x1="-66.04" y1="68.58" x2="-60.96" y2="68.58" width="0.1524" layer="91"/>
<junction x="-66.04" y="68.58"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="243.84" y1="111.76" x2="256.54" y2="111.76" width="0.1524" layer="91"/>
<label x="243.84" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ADC6"/>
<wire x1="27.94" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<label x="20.32" y="27.94" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="ADC7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ADC7"/>
<wire x1="27.94" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<label x="20.32" y="25.4" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PD7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD7"/>
<wire x1="63.5" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="66.04" y="15.24" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN1"/>
<wire x1="172.72" y1="20.32" x2="185.42" y2="20.32" width="0.1524" layer="91"/>
<label x="175.26" y="20.32" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="AIN2/AENBL"/>
<wire x1="243.84" y1="71.12" x2="226.06" y2="71.12" width="0.1524" layer="91"/>
<label x="226.06" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD6"/>
<wire x1="63.5" y1="17.78" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<label x="66.04" y="17.78" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN2"/>
<wire x1="185.42" y1="17.78" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<label x="175.26" y="17.78" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="AIN1/APHASE"/>
<wire x1="243.84" y1="73.66" x2="226.06" y2="73.66" width="0.1524" layer="91"/>
<label x="226.06" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD5"/>
<wire x1="63.5" y1="20.32" x2="73.66" y2="20.32" width="0.1524" layer="91"/>
<label x="66.04" y="20.32" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN3"/>
<wire x1="172.72" y1="15.24" x2="185.42" y2="15.24" width="0.1524" layer="91"/>
<label x="175.26" y="15.24" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="VCC"/>
<wire x1="243.84" y1="86.36" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<label x="226.06" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD4"/>
<wire x1="73.66" y1="22.86" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<label x="66.04" y="22.86" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN4"/>
<wire x1="185.42" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<label x="175.26" y="12.7" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PD3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD3"/>
<wire x1="63.5" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<label x="66.04" y="25.4" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN5"/>
<wire x1="172.72" y1="10.16" x2="185.42" y2="10.16" width="0.1524" layer="91"/>
<label x="175.26" y="10.16" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PD2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD2"/>
<wire x1="73.66" y1="27.94" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<label x="66.04" y="27.94" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN6"/>
<wire x1="185.42" y1="7.62" x2="172.72" y2="7.62" width="0.1524" layer="91"/>
<label x="175.26" y="7.62" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC0"/>
<wire x1="63.5" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<label x="66.04" y="53.34" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN6"/>
<wire x1="185.42" y1="27.94" x2="172.72" y2="27.94" width="0.1524" layer="91"/>
<label x="175.26" y="27.94" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC1"/>
<wire x1="73.66" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<label x="66.04" y="50.8" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN5"/>
<wire x1="172.72" y1="30.48" x2="185.42" y2="30.48" width="0.1524" layer="91"/>
<label x="175.26" y="30.48" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC2"/>
<wire x1="63.5" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<label x="66.04" y="48.26" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN4"/>
<wire x1="185.42" y1="33.02" x2="172.72" y2="33.02" width="0.1524" layer="91"/>
<label x="175.26" y="33.02" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC3"/>
<wire x1="73.66" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<label x="66.04" y="45.72" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN3"/>
<wire x1="185.42" y1="35.56" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<label x="175.26" y="35.56" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC4"/>
<wire x1="63.5" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<label x="66.04" y="43.18" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN2"/>
<wire x1="172.72" y1="38.1" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<label x="175.26" y="38.1" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PC5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC5"/>
<wire x1="73.66" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<label x="66.04" y="40.64" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="AD" gate="G$1" pin="PIN1"/>
<wire x1="172.72" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<label x="175.26" y="40.64" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="PB2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB2"/>
<wire x1="63.5" y1="71.12" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<label x="66.04" y="71.12" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN6"/>
<wire x1="172.72" y1="53.34" x2="185.42" y2="53.34" width="0.1524" layer="91"/>
<label x="175.26" y="53.34" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="SEL"/>
<wire x1="248.92" y1="25.4" x2="238.76" y2="25.4" width="0.1524" layer="91"/>
<label x="238.76" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB1"/>
<wire x1="73.66" y1="73.66" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<label x="66.04" y="73.66" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN7"/>
<wire x1="185.42" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<label x="175.26" y="50.8" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="BIN2/BENBL"/>
<wire x1="243.84" y1="63.5" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<label x="226.06" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB0"/>
<wire x1="63.5" y1="76.2" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<label x="66.04" y="76.2" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN8"/>
<wire x1="185.42" y1="48.26" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<label x="175.26" y="48.26" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$0" pin="BIN1/BPHASE"/>
<wire x1="243.84" y1="66.04" x2="226.06" y2="66.04" width="0.1524" layer="91"/>
<label x="226.06" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="POWER" gate="G$1" pin="PIN6"/>
<wire x1="172.72" y1="73.66" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<label x="175.26" y="73.66" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<label x="172.72" y="-15.24" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="VCC"/>
<wire x1="172.72" y1="-15.24" x2="185.42" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$0" pin="VIN"/>
<label x="-119.38" y="76.2" size="1.778" layer="95"/>
<wire x1="-101.6" y1="76.2" x2="-109.22" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="76.2" x2="-124.46" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="73.66" x2="-124.46" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="P$1"/>
<wire x1="-109.22" y1="73.66" x2="-109.22" y2="76.2" width="0.1524" layer="91"/>
<junction x="-109.22" y="76.2"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="AREF"/>
<label x="20.32" y="60.96" size="1.016" layer="95" font="vector"/>
<wire x1="27.94" y1="60.96" x2="12.7" y2="60.96" width="0.1524" layer="91"/>
<wire x1="12.7" y1="60.96" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IOH" gate="G$1" pin="PIN1"/>
<wire x1="185.42" y1="66.04" x2="172.72" y2="66.04" width="0.1524" layer="91"/>
<label x="175.26" y="66.04" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD0"/>
<wire x1="73.66" y1="33.02" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<label x="66.04" y="33.02" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN8"/>
<wire x1="185.42" y1="2.54" x2="172.72" y2="2.54" width="0.1524" layer="91"/>
<label x="175.26" y="2.54" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="TXO"/>
<wire x1="185.42" y1="-12.7" x2="172.72" y2="-12.7" width="0.1524" layer="91"/>
<label x="172.72" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD1"/>
<wire x1="63.5" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<label x="66.04" y="30.48" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="IOL" gate="G$1" pin="PIN7"/>
<wire x1="172.72" y1="5.08" x2="185.42" y2="5.08" width="0.1524" layer="91"/>
<label x="175.26" y="5.08" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="RXI"/>
<wire x1="185.42" y1="-10.16" x2="172.72" y2="-10.16" width="0.1524" layer="91"/>
<label x="172.72" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="DTR"/>
<wire x1="185.42" y1="-7.62" x2="172.72" y2="-7.62" width="0.1524" layer="91"/>
<label x="172.72" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<label x="132.08" y="55.88" size="1.778" layer="95"/>
<wire x1="127" y1="55.88" x2="142.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<wire x1="106.68" y1="91.44" x2="17.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<label x="25.4" y="91.44" size="1.016" layer="95" font="vector"/>
<pinref part="U1" gate="G$1" pin="AVCC"/>
<wire x1="27.94" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<wire x1="17.78" y1="55.88" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="91.44" width="0.1524" layer="91"/>
<junction x="17.78" y="91.44"/>
<pinref part="C3" gate="G$1" pin="P$1"/>
<wire x1="17.78" y1="91.44" x2="10.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="10.16" y1="91.44" x2="0" y2="91.44" width="0.1524" layer="91"/>
<wire x1="0" y1="91.44" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="91.44" x2="-30.48" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="91.44" x2="-30.48" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<wire x1="-10.16" y1="83.82" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="P$1"/>
<wire x1="0" y1="83.82" x2="0" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="10.16" y1="83.82" x2="10.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="93.98" x2="-30.48" y2="91.44" width="0.1524" layer="91"/>
<junction x="-30.48" y="91.44"/>
<junction x="10.16" y="91.44"/>
<junction x="0" y="91.44"/>
<junction x="-10.16" y="91.44"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="27.94" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<junction x="17.78" y="76.2"/>
<wire x1="154.94" y1="76.2" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<wire x1="157.48" y1="76.2" x2="157.48" y2="91.44" width="0.1524" layer="91"/>
<wire x1="157.48" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<junction x="106.68" y="91.44"/>
</segment>
<segment>
<pinref part="ISP" gate="G$1" pin="PIN2"/>
<wire x1="208.28" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<label x="210.82" y="121.92" size="1.016" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="VCC"/>
<wire x1="261.62" y1="33.02" x2="261.62" y2="38.1" width="0.1524" layer="91"/>
<wire x1="261.62" y1="38.1" x2="274.32" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$0" pin="VOUT"/>
<wire x1="-76.2" y1="76.2" x2="-71.12" y2="76.2" width="0.1524" layer="91"/>
<label x="-73.66" y="76.2" size="1.778" layer="95"/>
<pinref part="C13" gate="G$1" pin="P$1"/>
<wire x1="-71.12" y1="76.2" x2="-66.04" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="76.2" x2="-60.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="73.66" x2="-71.12" y2="76.2" width="0.1524" layer="91"/>
<junction x="-71.12" y="76.2"/>
<pinref part="C14" gate="G$1" pin="P$1"/>
<wire x1="-66.04" y1="73.66" x2="-66.04" y2="76.2" width="0.1524" layer="91"/>
<junction x="-66.04" y="76.2"/>
</segment>
<segment>
<pinref part="POWER" gate="G$1" pin="PIN2"/>
<wire x1="172.72" y1="83.82" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<label x="175.26" y="83.82" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="ANODE"/>
<pinref part="R2" gate="G$1" pin="P$2"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$0" pin="AOUT1"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="297.18" y1="86.36" x2="302.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="302.26" y1="86.36" x2="302.26" y2="93.98" width="0.1524" layer="91"/>
<junction x="302.26" y="86.36"/>
<pinref part="D5" gate="G$1" pin="CATHODE"/>
<wire x1="302.26" y1="93.98" x2="320.04" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="ANODE"/>
<wire x1="320.04" y1="93.98" x2="320.04" y2="88.9" width="0.1524" layer="91"/>
<junction x="320.04" y="93.98"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U2" gate="G$0" pin="AOUT2"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="297.18" y1="83.82" x2="302.26" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="317.5" y1="83.82" x2="302.26" y2="83.82" width="0.1524" layer="91"/>
<junction x="302.26" y="83.82"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U2" gate="G$0" pin="BOUT1"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="297.18" y1="78.74" x2="302.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="302.26" y1="78.74" x2="317.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="302.26" y="78.74"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U2" gate="G$0" pin="BOUT2"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="297.18" y1="76.2" x2="302.26" y2="76.2" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="CATHODE"/>
<wire x1="320.04" y1="73.66" x2="317.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="317.5" y1="73.66" x2="314.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="314.96" y1="73.66" x2="314.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="314.96" y1="76.2" x2="302.26" y2="76.2" width="0.1524" layer="91"/>
<junction x="302.26" y="76.2"/>
<wire x1="317.5" y1="68.58" x2="317.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="317.5" y="73.66"/>
<pinref part="D3" gate="G$1" pin="ANODE"/>
<wire x1="320.04" y1="68.58" x2="317.5" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="ANT"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="274.32" y1="20.32" x2="276.86" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="9V" class="0">
<segment>
<pinref part="U2" gate="G$0" pin="VM"/>
<wire x1="243.84" y1="83.82" x2="220.98" y2="83.82" width="0.1524" layer="91"/>
<label x="226.06" y="83.82" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
<wire x1="220.98" y1="83.82" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<wire x1="215.9" y1="81.28" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="P$1"/>
<wire x1="210.82" y1="81.28" x2="210.82" y2="83.82" width="0.1524" layer="91"/>
<wire x1="210.82" y1="83.82" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<junction x="215.9" y="83.82"/>
<pinref part="C10" gate="G$1" pin="P$1"/>
<wire x1="220.98" y1="81.28" x2="220.98" y2="83.82" width="0.1524" layer="91"/>
<junction x="220.98" y="83.82"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="243.84" y1="114.3" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<label x="243.84" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="327.66" y1="78.74" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="330.2" y1="78.74" x2="330.2" y2="73.66" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="ANODE"/>
<wire x1="330.2" y1="73.66" x2="325.12" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="330.2" y="73.66"/>
<pinref part="D3" gate="G$1" pin="CATHODE"/>
<wire x1="325.12" y1="68.58" x2="330.2" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="ANODE"/>
<pinref part="D4" gate="G$1" pin="CATHODE"/>
<wire x1="325.12" y1="93.98" x2="325.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="325.12" y1="91.44" x2="325.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="330.2" y1="91.44" x2="330.2" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="P$2"/>
<wire x1="330.2" y1="83.82" x2="327.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="325.12" y1="91.44" x2="330.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="325.12" y="91.44"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
