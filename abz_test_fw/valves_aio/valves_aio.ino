#include "LoRaWAN.h"

#define LED 13

#define BOOST_EN 13
#define DRV_EN 12
#define A_ON 8
#define A_OFF 9
#define B_ON 11
#define B_OFF 10


const char *appEui  = "70B3D57ED0010D89";

// full setup
const char *devEui  = "000964CE3AE1AF8E"; // full setup
const char *appKey  = "D6E803AFF2A3E8AE3A59B3C874672F59"; // full setup

// bare board
//const char *devEui  = "0024436543A7102A"; // bare board
//const char *appKey  = "354F289FAFF203812933345E05BFDCD9"; // bare board


void setup( void )
{
    Serial.begin(9600);

    // initialize pins
    pinMode(BOOST_EN, OUTPUT);
    pinMode(DRV_EN, OUTPUT);
    pinMode(A_ON, OUTPUT);
    pinMode(A_OFF, OUTPUT);
    pinMode(B_ON, OUTPUT);
    pinMode(B_OFF, OUTPUT);
  
    // start up the driver and boost converter
    digitalWrite(DRV_EN, LOW);
    digitalWrite(BOOST_EN, LOW);

    // flash the turn-on signal
    digitalWrite(BOOST_EN, HIGH);
    delay(100);
    digitalWrite(BOOST_EN, LOW);
    delay(100);
    digitalWrite(BOOST_EN, HIGH);
    delay(100);
    digitalWrite(BOOST_EN, LOW);

    // init output pins low
    digitalWrite(A_ON, LOW);
    digitalWrite(A_OFF, LOW);
    digitalWrite(B_ON, LOW);
    digitalWrite(B_OFF, LOW);

    //  Configure LoRaWan
    LoRaWAN.begin(US915);
    LoRaWAN.setSubBand(7);
    // LoRaWAN.setDutyCycle(false);
    // LoRaWAN.setAntennaGain(-2.5);
    LoRaWAN.setADR(false);
    LoRaWAN.setDataRate(0);
    LoRaWAN.setTxPower(14);
    LoRaWAN.onReceive(receiveCallback);
    
    LoRaWAN.joinOTAA(appEui, appKey, devEui);
    Serial.println("JOIN( )");
}

bool state = false;

void receiveCallback(void)
{
    Serial.print("RECEIVE( ");
    Serial.print("RSSI: ");
    Serial.print(LoRaWAN.lastRSSI());
    Serial.print(", SNR: ");
    Serial.print(LoRaWAN.lastSNR());

    if (LoRaWAN.parsePacket())
    {
        uint32_t size;
        uint8_t data[256];

        size = LoRaWAN.read(&data[0], sizeof(data));

        if (size)
        {
            data[size] = '\0';

            Serial.print(", PORT: ");
            Serial.print(LoRaWAN.remotePort());
            Serial.print(", DATA: \"");
            Serial.print((const char*)&data[0]);
            Serial.println("\"");

            if (size >= 3 && data[0] == '1') {
              // version and size look good
              if (data[1] == 'S') {
                digitalWrite(LED, HIGH);
                // turn on a valve
                if (data[2] == '1') {
                  Serial.println("Opening port A");
                  pulse_valve(A_ON);
                } else if (data[2] == '2') {
                  Serial.println("Opening port B");
                  pulse_valve(B_ON);
                } else {
                  Serial.println("Unrecognized port");
                }
              } else if (data[1] == 'E') {
                digitalWrite(LED, LOW);
                // turn off a valve
                if (data[2] == '1') {
                  Serial.println("Closing port A");
                  pulse_valve(A_OFF);
                } else if (data[2] == '2') {
                  Serial.println("Closing port B");
                  pulse_valve(B_OFF);
                } else {
                  Serial.println("Unrecognized port");
                }
              } else {
                Serial.println("Unrecognized command");
              }
            } else {
              Serial.println("Unknown message version");
            }
        }
    } else {
      Serial.println("Error parsing packet");
    }

    Serial.println(" )");
}

void loop( void )
{
    if (LoRaWAN.joined() && !LoRaWAN.busy())
    {
        Serial.print("TRANSMIT( ");
        Serial.print("TimeOnAir: ");
        Serial.print(LoRaWAN.getTimeOnAir());
        Serial.print(", NextTxTime: ");
        Serial.print(LoRaWAN.getNextTxTime());
        Serial.print(", MaxPayloadSize: ");
        Serial.print(LoRaWAN.getMaxPayloadSize());
        Serial.print(", DR: ");
        Serial.print(LoRaWAN.getDataRate());
        Serial.print(", TxPower: ");
        Serial.print(LoRaWAN.getTxPower(), 1);
        Serial.print("dbm, UpLinkCounter: ");
        Serial.print(LoRaWAN.getUpLinkCounter());
        Serial.print(", DownLinkCounter: ");
        Serial.print(LoRaWAN.getDownLinkCounter());
        Serial.println(" )");

        LoRaWAN.beginPacket();
        char msg[] = "Hello, World!";
        int i;
        for (i = 0; i < strlen(msg); i++) {
          LoRaWAN.write(msg[i]);
        }
        LoRaWAN.endPacket();
    }

    delay(10000);
}

void pulse_valve(uint8_t pin) {
  digitalWrite(pin, HIGH);

  // start up the driver and boost converter
  digitalWrite(BOOST_EN, HIGH);  
  digitalWrite(DRV_EN, HIGH);
  delay(50);
  digitalWrite(DRV_EN, LOW);
  digitalWrite(BOOST_EN, LOW);

  digitalWrite(pin, LOW);
}
