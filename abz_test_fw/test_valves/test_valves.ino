
#define BOOST_EN 13
#define DRV_EN 12
#define A_ON 8
#define A_OFF 9
#define B_ON 11
#define B_OFF 10

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize pins
  pinMode(BOOST_EN, OUTPUT);
  pinMode(DRV_EN, OUTPUT);
  pinMode(A_ON, OUTPUT);
  pinMode(A_OFF, OUTPUT);
  pinMode(B_ON, OUTPUT);
  pinMode(B_OFF, OUTPUT);

  // start up the driver and boost converter
  digitalWrite(DRV_EN, HIGH);
  digitalWrite(BOOST_EN, HIGH);
  Serial.begin(115200);

  delay(1000);
}

// the loop function runs over and over again forever
void loop() {
  pulse_valve(A_ON);
  pulse_valve(B_ON);
  delay(1000);
  pulse_valve(A_OFF);
  pulse_valve(B_OFF);
  delay(1000);
}

void pulse_valve(uint8_t pin) {
  /// Serial.print(pin);
  digitalWrite(pin, HIGH);
  // Serial.print(" HIGH");
  delay(50);
  digitalWrite(pin, LOW);
  // Serial.println(" LOW");
}

