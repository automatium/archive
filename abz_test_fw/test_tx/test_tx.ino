// #include "Buffer.h"
#include "RadioConfig.cpp"
#include "RollingBuffer.cpp"
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>

#define SERVER_ADDRESS 1

// commands and messages
#define CMD_MAX_LEN 25

#define IPS(i) i, sizeof(i) - 1

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, SERVER_ADDRESS);

void matchMsg() {}

bool status = false;

void setup() {
  Serial.begin(115200);
  while (!Serial)
    ; // wait

  Serial.println("INIT_START");

  if (!manager.init()) {
    status = true;
    Serial.println("INIT_ERR");
  }
  else
    Serial.println("INIT_OK");

  // configure driver
  driver.setModemConfig(MODEM_PRESET);
  driver.setFrequency(FREQ);
  driver.setPreambleLength(TX_PREAMBLE, ACK_PREAMBLE);
}

// Serial input buffer
RollingBuffer<CMD_MAX_LEN, true> serin;
void loop() {
  // prepare to send
  unsigned long start = millis();

  while (millis() - start < 500) {
    if (manager.sendto((uint8_t *)IPS("HELLO"), 3)) {
      Serial.print(".");
    } else {
      Serial.println("\nTX Error!");
    }
  }

  Serial.println();

  // // print status
  // if (status) {
  //   // wait for the ACK packet
  //   start = millis();
  //   uint8_t ackbuf[2] = {0, 0};
  //   uint8_t len = sizeof(ackbuf);
  //   uint8_t from;
  //   while (millis() - start < ACK_TIMEOUT) {
  //     if (manager.recvfrom(ackbuf, &len, &from) &&
  //         from == dst_addr && ackbuf[0] == 'O' &&
  //         ackbuf[1] == 'K') {
  //       Serial.println("\nSEND_OK");
  //       // return to mail loop
  //       return;
  //     }
  //   }
  //
  //   // must not have gotten an ACK
  //   Serial.println("\nSEND_ERR");
  //
  // } else {
  //   Serial.println("\nSEND_ERR");
  // }
}
