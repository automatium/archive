import { data } from 'ttn'
import Koa from 'koa'
import Router from 'koa-router'
import logger from 'koa-logger'
import chalk from 'chalk'

const appID = 'au-test-2'
const accessKey = 'ttn-account-v2.X7L0gIwERAzq9E_DkOwC9AbOL26MJJ1oWQcX2uFD77I'

const main = async () => {
  const app = new Koa()
  const router = new Router()

  // const client = await data(appID, accessKey)
  //
  // // uplink event cache
  // const eventCache = {}
  // client.on('uplink', function(devId, message) {
  //   // init the slot, if it's new
  //   if (typeof eventCache[devId] === 'undefined') {
  //     eventCache[devId] = []
  //   }
  //
  //   // insert latest message
  //   eventCache[devId].push(message)
  // })

  // routes
  router.get('/send/:device/:port/:msg', async (ctx) => {
    logAction(
      `Processing command to send ${ctx.params.msg} to ${ctx.params.device}:${
        ctx.params.port
      }`,
    )

    const client = await data(appID, accessKey)

    client.send(
      ctx.params.device,
      Buffer.from(ctx.params.msg, 'utf8'),
      // '01FF',
      Number(ctx.params.port),
      true,
      'last',
    )

    ctx.status = 200
  })

  // router.get('/events/all', async (ctx) => {
  //   logAction(`Sending full event list`, eventCache)
  //
  //   ctx.body = JSON.stringify(eventCache)
  //
  //   ctx.status = 200
  // })
  //
  // router.get('/events/latest/:device', async (ctx) => {
  //   logAction(`Sending latest event from ${ctx.params.device}`)
  //
  //   const deviceEvents = eventCache[ctx.params.device]
  //   const latestEvent = deviceEvents[deviceEvents.length - 1]
  //
  //   console.log(latestEvent)
  //
  //   ctx.body = JSON.stringify(lastestEvent)
  //   ctx.status = 200
  // })

  // request handler
  app.use(router.routes())
  app.use(router.allowedMethods())

  // start the server
  const listenPort = process.env.PORT || 4002
  logAction(`Listening on port *:${listenPort}`)
  app.listen(listenPort)
}

function logAction(...props) {
  return console.log(chalk.blue('==>', ...props))
}

main()
