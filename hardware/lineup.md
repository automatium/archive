# Automatium Hardware Lineup

## System Controller

**Budget**

Simple controller with stock polycase enclosure and cable glands.

**Pro**

* Outdoor enclosure comes standard.
* High-gain pcb patch antenna.
* Supports PoE power
* Second ethernet port with PoE output for chaining devices.

## All-in-one modules

Everything needed for a particular use-case combined into a single package.

Features:
* Integrated solar panel
* Pluggable terminal blocks
* Wires exit from the bottom corner
* Snap-on bottom cover
* Electronics are mounted on a sled
* Quick-remove system similar to Ubiquiti PoE modules.
* Lora wireless
* ESP32 for Wifi & Bluetooth

Modules:
* Solar Valve Manager (2 valves & 2 moisture sensors)
* Power Controller (temp/humidity with ACST1635-8FP AC/DC switches)
* Weather Station (Wind speed, wind direction, temperature, humidity, rainfall, water flow)
* Portable Ph/sap meter

## Pro Network

Everything communicates over standard ethernet.

* 2 pairs of 24v passive PoE
* 1 pair of 10BASE-T ethernet, allowing the device to be used with a traditional network.
* 1 pair for CAN

Each device has 2-4 rj-45 jacks with ethernet avaialble on port 1.  CAN and power are also broken out to separate terminal blocks.  There is also a USB-C port for debugging and programming.

Lots of modules:

* Solar module
* AC valve output
* DC valve output
* LoRa Comms
* Wifi & Bluetooth comms
* LCD + touchscreen
* Light Level
* Temp/Humidity
* Soil Moisture
* Flow Meter
* GPS Module
* AC switch
* DC Switch
* Motion Sensor
* Current Sensor
* Voltage Reader
* Light level sensor

Given the huge quantity of modules it makes sense to create a standardized enclosure and comms/cpu module.  The standard PCB would have an expansion footprint like the BBB where other add-on boards could plug in.  The only thing that would change between modules is the I/O shield.

Large module ($38):

* Ethernet Controller ($3)
* WizNet Internet Engine ($3)
* 2x 2-port ethernet jack ($2)
* USB-C port ($1)
* Ethernet Transformer
* STM32L4A6 ($8)
* Misc ($5)
* Enclosure ($15)

That is more expensive than a Raspberry Pi (!)  Something is wrong with this picture.

Small potted module ($10)
* CAN-enabled MCU ($3)
* Potted Enclosure ($5)
* PCB + components ($2)
